program wdyn
    implicit none
    integer, parameter :: dpr=kind(1.d0)

    integer :: u
    integer :: i, j, k, l, m
    integer :: nat
    integer :: step
    integer :: nstat
    integer :: istat
    integer :: buff
    integer :: idx
    integer :: idr
    integer :: nfiles
    integer :: ierr
    integer :: npath
    integer :: irk
    integer :: istat_old
    integer :: stati
    integer :: statj
    integer :: iseed
    integer :: ngw
    integer :: nflo
    integer :: nintx
    integer :: ntest

    integer, allocatable, dimension(:) :: ibuf
    
    real(dpr), parameter :: con_mass = 1822.888515
    real(dpr), parameter :: au2debye = 2.541747760
    
    real(dpr) :: dt
    real(dpr) :: t
    real(dpr) :: t_tot
    real(dpr) :: ekin
    real(dpr) :: etot
    real(dpr) :: rbuff
    real(dpr) :: cordx, cordy, cordz
    real(dpr) :: atmass
    real(dpr) :: ams
    real(dpr) :: velx, vely, velz
    real(dpr) :: cx, cy, cz

    real(dpr), allocatable, dimension(:) :: fbuf
    real(dpr), allocatable, dimension(:) :: epot
    real(dpr), allocatable, dimension(:) :: q
    real(dpr), allocatable, dimension(:) :: v
    real(dpr), allocatable, dimension(:) :: mass
    real(dpr), allocatable, dimension(:) :: pop
    real(dpr), allocatable, dimension(:) :: shprob

    real(dpr), allocatable, dimension(:,:) :: tpar
    real(dpr), allocatable, dimension(:,:) :: trdip_debye
    real(dpr), allocatable, dimension(:,:) :: mu
    real(dpr), allocatable, dimension(:,:) :: ovl
    real(dpr), allocatable, dimension(:,:) :: ham_exc

    character(len=200) :: string
    character(len=200) :: cbuff

    logical :: check_stop
    logical :: file_exists
    logical :: check_dip
    logical :: check_exciton
    logical :: check_surf
    logical :: check_dyn

    nintx = 0
    nflo = 0

    check_exciton = .false.
    inquire(file="ham_exc", exist=check_exciton)

    ! Read control.d
    open(newunit=u, file='control.d', status='old')
    rewind u
    read(u,*) nat, step, nstat, istat, buff, buff, dt, t, t_tot
    close(u)

    ! Size of the ibuf (array with all the integers) and 
    ! fbuf (array with all the reals) arrays
    idx = 8
    if (check_exciton) then
       idr = 4 + (nat*3*2) + nstat + 2*(nstat*nstat) + &
             & nstat + (nstat*3) + (((nstat*nstat) - &
             & nstat)/2 + nstat) + nstat
    else
       idr = 4 + (nat*3*2) + nstat + (nstat*nstat) + &
             & nstat + (nstat*3) + nstat
    end if

    allocate( ibuf(idx) )
    ibuf = 0
    allocate( fbuf(idr) )
    fbuf = 0._dpr

    ! 1.INT - Recond the step
    nintx = nintx + 1
    ibuf(nintx) = step

    ! 2.INT - Record then ntest = 0 (no reaction);
    !                           = 2 (the internal coord. has crossed the threshold S);
    !                           = 3 (no reaction, total number of steps reached);
    
    open(newunit=u, file='ss-result', status='unknown')
    call search("STOP", string, u, check_stop)
    if (check_stop .eqv. .true.) then
       ntest = 2
    else if (t .eq. t_tot) then
       ntest = 3
    else
       ntest = 0
    end if

    nintx = nintx + 1
    ibuf(nintx) = ntest

    ! 3.INT - Record npath
    npath = 0
    nintx = nintx + 1
    ibuf(nintx) = npath

    ! 4.INT - Record current step
    nintx = nintx + 1
    ibuf(nintx) = istat

   ! 5.INT - Record irk = 0 (no hopping)
   !                    = 1 (surface hopping)
   !                    = 2 (state switch)
   !                    = 3 (surface hopping following a state switch) ***disabled***
   !                    = 4 (insufficient Ecin for surface hopping)
   !                    = 5 (classical Franck-Condon excitation at step KVERT) ***disabled***
   !                    = 6 (failure in energy conservation - reset Ekin) ***disabled***
   !                    = -1 (radiative surface hopping -- no energy conservation)
   !                    = -2 (field decay surface hopping -- no energy conservation)
   !
   ! I am taking this value from a nx file, so I don't know if those values above 
   ! are correct...
    open(newunit=u, file='irk', status='old')
    rewind u
    read(u,*) irk
    close(u)
 
    nintx = nintx + 1
    ibuf(nintx) = irk

   ! 6.INT - Record the traj number. However, this approach has only one trajectory 
   ! per input, so the number of the trajectory is always 1.
   nintx = nintx + 1
   ibuf(nintx) = 1

   ! => 7.INT - Record istat at the previous timestep:
   check_surf = .false.
   inquire(file="previous_surf", exist=check_surf)
   if (check_surf) then
      open(newunit=u, file='previous_surf', status='old')
      rewind u
      read(u,*) istat_old
      close(u)
   else
      istat_old = istat
   end if

   nintx = nintx + 1
   ibuf(nintx) = istat_old

   ! 8.INT - Record trdip_from_state. I set = 1 because the program
   ! always calculate the trdip for all the states.
   !
   nintx = nintx + 1
   ibuf(nintx) = 1

   ! 1.REAL - Record time
   nflo = nflo + 1
   fbuf(nflo) = t

   !2.REAL - Kinetic energy
   open(newunit=u, file='etot', status='old')
   rewind u
   read(u,*) etot
   close(u)

   allocate( epot(nstat) )
   epot(:) = 0._dpr
   open(newunit=u, file='epot', status='old')
   rewind u
   do i = 1, nstat
      read(u,*) epot(i)
   end do
   close(u)

   ekin = etot - epot(istat)
   nflo = nflo + 1
   fbuf(nflo) = ekin

   !3.REAL - Active potential energy
   nflo = nflo + 1
   fbuf(nflo) = epot(istat)

   !4.REAL - Total energy
   nflo = nflo + 1
   fbuf(nflo) = etot

   !5.REAL - Record the cartesian coords. and momenta

   allocate( q(nat*3), v(nat*3), mass(nat) )
   q(:) = 0._dpr
   v(:) = 0._dpr
   mass(:) = 0._dpr

   open(newunit=u, file='geom', status='old', form='formatted')
   rewind u
   k = 0
   do i = 1, nat
      read(u,*) cbuff, rbuff, cordx, cordy, cordz, atmass
      k = k + 1
      q(k) = cordx
      k = k + 1
      q(k) = cordy
      k = k + 1
      q(k) = cordz
      mass(i) = atmass
   end do
   close(u)
   open(newunit=u, file='veloc', status='old', form='formatted')
   rewind u
   l = 0
   do i = 1, nat
      read(u,*) velx, vely, velz
      l = l + 1
      v(l) = velx
      l = l + 1
      v(l) = vely
      l = l + 1
      v(l) = velz
   end do
   close(u)
   j = 1
   do l = 1, nat
      m = j + 2
      ams = mass(l) * con_mass
      do i = j, m
         nflo = nflo + 1
         fbuf(nflo) = q(i)
         nflo = nflo + 1
         fbuf(nflo) = v(i) * ams
      end do
      j = j + 3
   end do

   !6.REAL - CI energies
   do i = 1, nstat
      nflo = nflo + 1
      fbuf(nflo) = epot(i)
   end do

   !7.REAL - Record th eigenvectos of the exciton Hamiltonian (if exists)
   file_exists = .false.
   inquire(file="old_eigenvectors.txt", exist=file_exists)
   if (file_exists .eqv. .true.) then
      allocate( tpar(nstat,nstat) )
      tpar = 0._dpr
      open(newunit=u, file="old_eigenvectors.txt", status='old')
      rewind u
      do i = 1, nstat
         read(u,*) (tpar(i,j), j = 1, nstat)
      end do
      close(u)
      do i = 1, nstat
         do j = 1, nstat
            nflo = nflo + 1
            fbuf(nflo) = tpar(j,i)
         end do
      end do
    end if

    !8.REAL - Record the population of the adiabatic states
    allocate( pop(nstat) )
    pop(:) = 0._dpr
    open(newunit=u, file='pop_curr', status='old')
    rewind u
    read(u,*) buff, buff, buff, pop(1), rbuff
    do i = 2, nstat
       read(u,*) pop(i)
    end do
    close(u)
    do i = 1, nstat
       nflo = nflo + 1
       fbuf(nflo) = pop(i)
    end do

    !9.REAL - Record the transition dipole moments (DEBYE)
    if (check_exciton .eqv. .true.) then
       ! -- exciton dynamics
       allocate( trdip_debye((nstat-1),3) )
       trdip_debye(:,:) = 0._dpr
       open(newunit=u, file='trdip_exc', status='old')
       rewind u
       do i = 1, nstat-1
          read(u,*) (trdip_debye(i,j), j = 1, 3)
       end do
       close(u)
       !Record the ground state
       do i = 1, 3
          nflo = nflo + 1
          fbuf(nflo) = 0._dpr
       end do
       !Record the excited states
       do i = 1, nstat-1
          do j = 1, 3
             nflo = nflo + 1
             fbuf(nflo) = trdip_debye(i,j)
          end do
       end do
    else
       ! -- mopac dynamics
       j = 0
       do i = 1, nstat
          j = j + i
       end do
       allocate( mu((nstat-1), 3) )
       mu(:,:) = 0._dpr
       open(newunit=u, file='mopac.out', status='old')
       rewind u
       call search("Dipoles (au)", string, u, check_dip)
       read(u,*)
       k = 0
       do i = 1, j
          read(u,*) stati, statj, cx, cy, cz
          if (stati .ne. 1 .and. statj .eq. 1) then
             k = k + 1
             mu(k,1) = cx
             mu(k,2) = cy
             mu(k,3) = cz
          end if
       end do
       close(u)
       mu = mu * au2debye
       !Record the ground state
       do i = 1, 3
          nflo = nflo + 1
          fbuf(nflo) = 0._dpr
       end do
       !Record the excited states
       do i = 1, nstat-1
          do j = 1, 3
             nflo = nflo + 1
             fbuf(nflo) = mu(i,j)
          end do
       end do
    end if

    !10.REAL - Overlap matrix (adiabatic basis)
    allocate( ovl(nstat, nstat) )
    ovl(:,:) = 0._dpr
    open(newunit=u, file='run_cioverlap.log', status='old')
    rewind u
    read(u,*)
    do i = 1, nstat
       read(u,*) (ovl(i,j), j = 1, nstat)
    end do
    close(u)
    do i = 1, nstat
       do j = 1, nstat
          nflo = nflo + 1
          fbuf(nflo) = ovl(j,i)
       end do
    end do

    !11.REAL - Record a triangular matrix of the exciton 
    !          hamiltonian
    if (check_exciton .eqv. .true.) then
        allocate( ham_exc(nstat, nstat) )
        open(newunit=u, file='ham_exc', status='old')
        rewind u
        do i = 1, nstat
           read(u,*) (ham_exc(i,j), j = 1, nstat)
        end do
        close(u)
        do i = 1, nstat
            do j = 1, i
               nflo = nflo + 1
               fbuf(nflo) = ham_exc(i,j)
           end do
        end do
    end if

    !12.REAL - FS trans. rate (tprob/dt, a.u.)
    allocate( shprob(nstat) )
    shprob = 0._dpr
    open(newunit=u, file='shprob', status='old')
    rewind u
    do i = 1, nstat
       read(u,*) shprob(i) 
    end do
    close(u)
    do i = 1, nstat
       nflo = nflo + 1
       fbuf(nflo) = shprob(i)
    end do

    iseed = 1
    ngw = 0

    inquire(file='../RESULTS/add_info.dyn', exist=check_dyn)

    if (check_dyn .eqv. .false. .and. step .ne. 0) then
        open(newunit=u, file='../RESULTS/add_info.dyn', status='new', &
             & form='unformatted', action='write')
        write(u) iseed, -1, 0
        write(u) 0, 0, 0, 1, 1
        call winf(nstat, nat, check_exciton)
        ngw = 1
        fbuf(1) = 0._dpr
        write(u) ngw, nintx, nflo, (ibuf(i), i = 1,nintx), (fbuf(i), i = 1,nflo)
        fbuf(1) = t
        ngw = step + 1
        write(u) ngw, nintx, nflo, (ibuf(i), i = 1,nintx), (fbuf(i), i = 1,nflo)
        close(u)
     else if (check_dyn .eqv. .false. .and. step .eq. 0) then
        ngw = step + 1
        open(newunit=u, file='../RESULTS/add_info.dyn', status='new', &
             & form='unformatted', action='write')
        write(u) iseed, -1, 0
        write(u) 0, 0, 0, 1, 1
        call winf(nstat, nat, check_exciton)
        ngw = step + 1
        write(u) ngw, nintx, nflo, (ibuf(i), i = 1,nintx), (fbuf(i), i = 1,nflo)
        close(u)
     else
        ngw = step + 1
        open(newunit=u, file='../RESULTS/add_info.dyn', status='unknown', &
            & form='unformatted', position='append')
        write(u) ngw, nintx, nflo, (ibuf(i), i = 1,nintx), (fbuf(i), i = 1,nflo)
        if (ntest .ne. 0) then
           write(u) iseed, -2, 0
        end if
        close(u)
    end if
    
    !if (step .eq. 1) then
    !    open(newunit=u, file='../RESULTS/add_info.dyn', status='new', &
    !         & form='unformatted', action='write')
    !    write(u) iseed, -1, 0
    !    write(u) 0, 0, 0, 1, 1
    !    close(u)
    !    call winf(nstat, nat, check_exciton)
    !end if

    !ngw = step + 1
    !open(newunit=u, file='../RESULTS/add_info.dyn', status='unknown', &
    !     & form='unformatted', position='append')
    !write(u) ngw, nintx, nflo, (ibuf(i), i = 1,nintx), (fbuf(i), i = 1,nflo)
    !if (ntest .ne. 0) then
    !   write(u) iseed, -2, 0
    !end if
    !close(u)

contains

subroutine search(targt, stringa, nfiles, conferma)
    character (len=*), intent (in)  :: targt
    character (len=*), intent (out) :: stringa
    integer, intent(in)  :: nfiles
    logical, intent(out) :: conferma

    integer :: icod

    intrinsic Index

    conferma=.false.
    do
       if (conferma) exit
       read(nfiles,'(A)',iostat=icod)stringa
       if (icod < 0) exit
       conferma=(Index(stringa,targt) /= 0)
    end do

    return
 end subroutine search

subroutine winf(nstat, nat, check_exc)
   implicit none
   
   integer, parameter :: ngwmax=999999
   integer, parameter :: dpr=kind(1.d0)

   integer :: u
   integer :: i
   integer :: nstat
   integer :: nat
   integer :: nintx
   integer :: n1
   integer :: nflo
   integer :: versinf
   integer :: norbs
   integer :: nmos
   integer :: ncf
   integer :: nstat_t
   integer :: inco
   integer :: iwrt
   integer :: atn
   integer :: isolv
   integer :: numatmm_act
   integer :: inicoord
   integer :: numfrz
   integer :: nlines
   integer :: io

   integer, allocatable, dimension(:) :: nat_p

   character(len=120) :: out_name

   character(len=200) :: string
   character(len=200) :: namefile
   character(len=2) :: dash

   character(len=10), allocatable, dimension(:) :: elemnt
   character(len=200), allocatable, dimension(:) :: mop_inp

   real(dpr), parameter :: con_mass=1822.888515

   real(dpr) :: atnumb
   real(dpr) :: rbuff
   real(dpr) :: mass

   real(dpr), allocatable, dimension(:) :: atmau

   logical :: check_exc

   !Intrinsic Functions .. 
   intrinsic Index, Trim

   allocate( elemnt(nat) )
   elemnt(:) =  ""
   allocate( nat_p(nat) )
   nat_p(:) = 0
   allocate( atmau(nat) )
   atmau(:) = 0._dpr

   open(newunit=u, file='geom', status='old', form='formatted')
   rewind u
   do i = 1, nat
      read(u,*) elemnt(i), atnumb, rbuff, rbuff, rbuff, mass
      atn = INT(atnumb)
      nat_p(i) = atn
      atmau(i) = mass * con_mass
   end do
   close(u)

   if (check_exc .eqv. .false.) then
      ! --Checks the number of lines mopac input
      nlines = 0
      open(newunit=u, file='mopac.dat')
      do
         read(u,*,iostat=io)
         if (io /= 0) exit
         nlines = nlines + 1
      end do
      close(u)
      allocate( mop_inp(nlines) )
      mop_inp(:) = ""
      open(newunit=u, file='mopac.dat', status='old')
      rewind u
      do i = 1, nlines 
         read(u,"(a)") mop_inp(i)
      end do
      close(u)
   end if

   open(newunit=u, file='../RESULTS/add_info.inf', form='formatted', &
        & status='unknown')
   rewind u
   versinf = 160
   write(u,"(a,i10)") " Versione =", versinf
   !
   norbs = 1
   nmos = 1
   ncf = 1
   nstat_t = nstat
   inco = 0
   iwrt = 0
   write (u,"(a)") "# numat, norbs, nmos, ncf, nstat_t, nstati, inco, dynwrt"
   write (u,"(8i9)") nat, norbs, nmos, ncf, nstat_t, nstat, inco, iwrt

   write (u,"(a)") "# atom n.  element    Z    atomic mass (a.u)"
   do i = 1, nat
      write (u,10) i, elemnt(i), nat_p(i), atmau(i)
   end do
   !  numatmm_act = total number of MM atoms - frozen MM atoms
   !  only p and q's of numatm_act MM atoms are written to file .dyn
   numatmm_act = 0
   isolv = 0
   inicoord = 0
   numfrz = 0
   write (u,"(a)") "# isolv   numatmm_act   num_mol_solv     inicoord    numfrz"
   write (u,"(5i9)") isolv, numatmm_act, 1, inicoord, numfrz

   !inquire (u,NAME = fdyn)
   dash=" -"
   write (u,"(//80a)")("-",i=1,80)
   write (u,"(5x,a,/5x,a/)") &
           & "Information recorded in file add_info.dyn", &
           & "(all data in a.u., unless otherwise specified)"
   write (u,"(a,I8,a/a/)") &
        & " Structure of the records:   (records with ngw within 1 and", &
        & ngwmax-1, ")", " ngw,nintx,nflo,(ibuf(i),i=1,nintx),(fbuf(i),i=1,nflo)"

   !! Integer variables (ibuf)
   write (u,"(a)") " ibuf contains:"
   nintx = 1
   write (u,10000) nintx, "nc = number of time steps"
   nintx = nintx + 1
   write (u,"(i9,2x,a/11x,a/11x,a/11x,a)") &
        & nintx, "ntest = 0 (no reaction)", &
        & "        1 (the internal coord. has reached the threshold W) ***disable***", &
        & "        2 (the internal coord. has crossed the threshold S)", &
        & "        3 (no reaction, total number of steps reached)"
   nintx = nintx + 1
   write (u,10000) nintx, "npath (not used)"
   nintx = nintx + 1
   write (u,10000) nintx, "istat = index of the adiabatic surface"
   nintx = nintx + 1
   write (u,"(i9,2x,a/11x,a/11x,a,/11x,a,/11x,a,/11x,a,/11x,a,/11x,a,/11x,a)") &
        & nintx, "irk   = 0 (no hopping)", "      = 1 (surface hopping)", &
        & "      = 2 (state switch)", &
        & "      = 3 (surface hopping following a state switch) ***disabled***", &
        & "      = 4 (insufficient Ecin for surface hopping)", &
        & "      = 5 (classical Franck-Condon excitation at step KVERT) ***disabled***", &
        & "      = 6 (failure in energy conservation - reset Ekin) ***disabled***", &
        & "      = -1 (radiative surface hopping -- no energy conservation)",&
        & "      = -2 (field decay surface hopping -- no energy conservation)"
   nintx = nintx + 1
   write (u,10000) nintx, "ntz = trajectory number"
   nintx = nintx + 1
   write (u,10000) nintx, "istat_old = istat at the previous timestep"
   nintx = nintx + 1
   write (u,10000) nintx, "trdip_from_state = state from which trans. dip. are evaluated"

   !! Real variables (fbuf)
   write (u,"(/a)") " fbuf contains:"
   nflo = 1
   write (u,10001) nflo, dash, nflo, "_TIME_     = time (in fsec)"
   nflo = nflo + 1
   write (u,10001) nflo, dash, nflo, "_EKIN_     = kinetic energy"
   nflo = nflo + 1
   write (u,10001) nflo, dash, nflo, "_EPOT_     = potential energy"
   nflo = nflo + 1
   write (u,10001) nflo, dash, nflo, "_ETOT_     = total energy"
   n1 = nflo + 1
   nflo = nflo + (nat*6)
   write (u,10001) &
   & n1, dash, nflo, "_QP_       q(i),p(i),i=1,3n    = cartesian coords. and momenta"
   n1 = nflo + 1
   nflo = nflo + nstat
   write (u,10001) n1, dash, nflo, "_ECI_      eci(i),i=1,nstat_t   = CI energies"
   if (check_exc .eqv. .true.) then
      n1 = nflo + 1
      nflo = nflo + (nstat * nstat)
      write (u,10001) &
      & n1, dash, nflo, "_EXVEC_    tvec(i,j),i,j=1,nstat_t  = eigenvetors of the exciton Hamiltonian"
   end if
   n1 = nflo + 1
   nflo = nflo + nstat
   write (u,10001) n1, dash, nflo, &
         & "_POP_      pop(i),i=1,nstat_t   = populations of the adiabatic states"
   n1 = nflo + 1
   nflo = nflo + (3 * nstat)
   string = "_AMUT_     amut(i,j),j=1,3,i=1,nstat_t = transition dipole moments (DEBYE)"
   write (u,10001)  n1, dash, nflo,Trim(string)
   n1 = nflo + 1
   nflo = nflo + ( nstat * nstat )
   write (u,10001) &
   & n1, dash, nflo, "_TPAR_     tpar(i,j),i,j=1,nstat_t  = diabatic transf. (this step)"
   if (check_exc .eqv. .true.) then
      n1 = nflo + 1
      nflo = nflo + ( ( (nstat*nstat) - nstat )/ 2 + nstat )
      write (u,10001) &
      & n1, dash, nflo, "_EXHAM_    ham_tot(j,i),i=1,nstat_t; j=i,nstat_t = lower triangular matrix of the exciton Ham."
   end if
   n1 = nflo + 1
   nflo = nflo + nstat
   write (u,10001) &
   & n1, dash, nflo, "_TRANS_    shprob(i),i=1,nstat_t = FS trans. rate (tprob/dt, a.u.), referred to the previous timestep"

   
   !! trj reattiva
   write (u,"(//a,I8,a)") &
        & " For reactive trjs. only (records with ngw=", ngwmax, ")"
   write (u,"(a)") " ibuf contains:"
   write (u,10000) 1, "npath = reactive channel"
   write (u,10000) 2, "nfrag = number of fragments (products)"
   write (u,"(a)") " fbuf contains:"
   write (u,"(a)") " for each fragment, Etrasl, <Erot>, <Evib>"
   write (u,"(80a)")("-", i = 1,80)
   if (check_exc .eqv. .false.) then
      write (u,*)
      write (u,"(/a)")"------- Mopac input -------"
      do i = 1, nlines
         write(u,"(a)") mop_inp(i)
      end do
      deallocate( mop_inp ) 
   end if

   close(u)

   10  format(i6,6x,a2,2x,i6,2x,f20.10)
   12  format(i6,6x,a2,2x,i6,2x,f20.10,2x,3f18.9)
   10001 format (i9,a,i9,2x,a)
   10000 format (i9,2x,a)

end subroutine winf

end program wdyn

