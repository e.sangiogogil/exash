program inp_exc
!*************************************************************
   implicit none

   integer, parameter :: dpr=kind(1.d0)

   integer :: i, j, k
   integer :: nlines
   integer :: io
   integer :: nat
   integer :: nchrom
   integer :: natf
   integer :: nati
   integer :: nlines_ok
   integer :: nlines2
   integer :: u
   integer :: ibuf
   integer :: nlink
   integer :: nl
   integer :: nlines_list_exc

   integer, allocatable, dimension(:) :: ind
   integer, allocatable, dimension(:) :: atom_lnk_pos
   integer, allocatable, dimension(:,:) :: at_pos

   character(len=120) :: name_ind
   character(len=120) :: filename
   character(len=120) :: nameqm
   character(len=120) :: namemm
   character(len=120) :: string
   character(len=120) :: name_tot
   character(len=120) :: card

   character(len=200), allocatable, dimension(:) :: tot_txt
   character(len=200), allocatable, dimension(:) :: lab
   character(len=200), allocatable, dimension(:) :: name_mopac
   character(len=200), allocatable, dimension(:) :: tot_txt2
   character(len=200), allocatable, dimension(:) :: tot_txt3
   character(len=200), allocatable, dimension(:) :: atom_lnk_label
   character(len=200), allocatable, dimension(:) :: add_info

   real(dpr), allocatable, dimension(:,:) :: geom

   logical :: file_exists
   logical :: conferma
!*************************************************************
   
   nlines = 0 
   
   ! -- Checks the number of lines 
   open(newunit=u, file='layout_exc')
   do 
      read(u,*,iostat=io)
      if (io /= 0) exit
      nlines = nlines + 1
   end do
   close(u)

   ! --Reads the tinker file in order to take the geometry:
   open(newunit=u, file='fullMM_tnk.xyz', status='old', form='formatted')
   rewind u
   read(u,*) nat
   allocate( ind(nat), lab(nat), geom(nat,3) )
   ind(:) = 0
   lab(:) = ""
   geom(:,:) = 0._dpr
   do i = 1, nat
      read(u,*) ind(i), lab(i), (geom(i,j), j = 1, 3)
   end do
   close(u)
   
   !! Checks the number of lines in the "list_exc" file 
   nlines_list_exc = 0
   open(newunit=u, file='list_exc')
   do
      read(u,*,iostat=io)
      if (io /= 0) exit
      nlines_list_exc = nlines_list_exc + 1
   end do
   close(u)

   ! --Reads the list   
   open(newunit=u, file='list_exc', status='old', form='formatted')
   rewind u
   read(u,*) nchrom
   allocate( at_pos(nchrom,2) )
   at_pos(:,:) = 0
   do i = 1, nchrom
      read(u,*) (at_pos(i,j), j = 1, 2)
   end do
   
   ! --Read link atoms (if it is available)
   rewind u
   call search("LINK ATOMS", card, u, conferma)
   if ( conferma ) then
      do i = 1 , nchrom
         read(u,*) ibuf, nlink
         if (nlink .gt. 0) then
            allocate(atom_lnk_pos(nlink))
            atom_lnk_pos(:) = 0
            allocate(atom_lnk_label(nlink))
            atom_lnk_label(:) = ""
            do j = 1, nlink
               read(u,*) atom_lnk_pos(j), atom_lnk_label(j)
            end do
             do j = 1, nlink
               lab(atom_lnk_pos(j)) = atom_lnk_label(j)
            end do
             deallocate(atom_lnk_pos)
            deallocate(atom_lnk_label)
         end if
      end do
   end if
   close(u)
   
   ! -- Array which contains the name of the Mopac's files
   allocate( name_mopac(nchrom) )
   name_mopac(:) = ""
   do i = 1, nchrom
      write (name_ind, "(I10)") i
      write (filename, "(A)") 'exc_mop'
      name_mopac(i) =trim(adjustl(filename))//trim(adjustl(name_ind))//'.dat'
   end do
  
   ! --Ignores the blank lines
   allocate( tot_txt(nlines) )
   tot_txt(:) = ""

   nlines_ok = 0
   open(newunit=u, file='layout_exc', status='old', form='formatted')
   rewind u
   do i = 1, nlines
      read(u,'(a)') string
      if (len(trim(string)) == 0) then
         continue
      else
         nlines_ok = nlines_ok + 1
         tot_txt(nlines_ok) = string
      end if
   end do
   close(u)

   allocate( tot_txt2(nlines_ok) )
   tot_txt2(:) = ""

   do i = 1, nlines_ok
      tot_txt2(i) = tot_txt(i)
   end do
   
   ! --Checks if the file "layout_exc2" exists
   nlines2 = 0 
   inquire(file="layout_exc2", exist=file_exists)
   if (file_exists .eqv. .true.) then

      open(newunit=u, file='layout_exc2')
      do 
         read(u,*,iostat=io)
         if (io /= 0) exit
         nlines2 = nlines2 + 1
      end do
      close(u)

      allocate( tot_txt3(nlines2) )
      tot_txt3(:) = ""

      open(newunit=u, file='layout_exc2', status='old', form='formatted')
      rewind u
      do i = 1, nlines2
         read(u,'(a)') string
         tot_txt3(i) = string
      end do
      close(u)

   end if

   ! --Generates the input files
   do i = 1, nchrom
   
      open(newunit=u, file='list_exc', status='unknown', &
           & form='formatted')
      rewind u
      nl = 0
      call search("ADD INF", card, u, conferma)
      if ( conferma ) then
         if ( i .gt. 1 ) then
            do j = 1, i-1
               read(u,*) ibuf, nl
               if (nl .eq. 0) then
                  continue
               else
                  do k = 1, nl
                     read(u,*) 
                  end do
               end if
            end do
         end if
         read(u,*) ibuf, nl
         if (nl .eq. 0) then
            continue
         else
            allocate ( add_info(nl) )
            add_info(:) = ""
            do j = 1, nl
               read(u,'(a)') add_info(j)
            end do
         end if
      end if
      close(u)
      
      open(newunit=u, file=name_mopac(i), status='unknown', form='formatted')
         rewind u
 
         if (at_pos(i,1) .eq. 1) then

            natf = nat - at_pos(i,2)

            write (name_ind, "(I10)") natf
            write (filename, "(A)") 'QMMM='
            nameqm = trim(adjustl(filename))//trim(adjustl(name_ind))//' +'
            write(u,'(a)') nameqm

            do j = 1, nlines_ok
               write(u,'(a)') trim(tot_txt2(j))
            end do

            write(u,*)
            do j = 1, at_pos(i,2)
               write(u,100) lab(j), geom(j,1), 1,  geom(j,2), 1,  geom(j,3), 1
            end do

            if (file_exists .eqv. .true.) then
               write(u,*)
               do j = 1, nlines2
                  write(u,'(a)') trim(tot_txt3(j))
               end do
            end if
       
            if (nl .ne. 0) then
               write(u,*) ""
               do j = 1, nl
                  write(u,'(a)') trim(add_info(j))
               end do
               write(u,*) ""
               deallocate( add_info )
            end if
         
         else

            nati = at_pos(i,1) - 1
            natf = nat - at_pos(i,2)
            
            write (name_ind, "(I10)") natf
            write (filename, "(A)") 'QMMM='
            nameqm = trim(adjustl(filename))//trim(adjustl(name_ind))
            
            write (name_ind, "(I10)") nati
            write (filename, "(A)") 'MMQM='
            namemm = trim(adjustl(filename))//trim(adjustl(name_ind))//' +'

            name_tot = trim(adjustl(nameqm))//' '//trim(adjustl(namemm))

            write(u,'(a)') trim(name_tot)
             
            do j = 1, nlines_ok
               write(u,'(a)') trim(tot_txt2(j))
            end do
            write(u,*)

            do j = at_pos(i,1), at_pos(i,2)
               write(u,100) lab(j), geom(j,1), 1,  geom(j,2), 1,  geom(j,3), 1
            end do
            
            if (file_exists .eqv. .true.) then
               write(u,*)
               do j = 1, nlines2
                  write(u,'(a)') trim(tot_txt3(j))
               end do
            end if

            if (nl .ne. 0) then
               write(u,*) ""
               do j = 1, nl
                  write(u,'(a)') trim(add_info(j))
               end do
               write(u,*) ""
               deallocate( add_info )
            end if
    
         end if

      close(u)

   end do
   
100 format (a5, 1x, *(f12.6, i2))

contains

subroutine search(targt, stringa, nfiles, conferma)
   implicit none
   character (len=*), intent (in)  :: targt
   character (len=*), intent (out) :: stringa
   integer, intent(in)  :: nfiles
   logical, intent(out) :: conferma

   integer :: icod

   intrinsic Index

   conferma=.false.
   do
      if (conferma) exit
      read(nfiles,'(A)',iostat=icod)stringa
      if (icod < 0) exit
      conferma=(Index(stringa,targt) /= 0)
   end do

   return
end subroutine search

end program inp_exc
