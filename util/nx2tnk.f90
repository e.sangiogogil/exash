program nx2tnk

   implicit none
 
   !*******************************************************************
   integer, parameter :: dpr = kind(1.d0)

   integer :: i, j
   integer :: u
   integer :: nat

   integer, allocatable, dimension(:) :: vecnumb
   integer, allocatable, dimension(:) :: atype

   integer, allocatable, dimension(:,:) :: lnk

   real(dpr), parameter :: bohr = 0.529177210903

   real(dpr) :: rbuff

   real(dpr), allocatable, dimension(:,:) :: coord

   character(len=200) :: cbuff
   character(len=200) :: line

   character(len=200), allocatable, dimension(:) :: coord_one
   character(len=200), allocatable, dimension(:) :: coord_two
   character(len=200), allocatable, dimension(:) :: veclab
   !*******************************************************************
  
   ! 
   ! => Reads the old tinker file in order to extract the numb of atoms,
   ! atom types and link atoms
   !

   ! -- 1°: Reads all the rows of the "file_tnk.xyz" file as string
   !        and add "zeros" in the end of each line;
   !   
   ! -- 2°: Reads all the coluns of the strings.

   open (newunit=u, file='fullMM_tnk.xyz', status="old", form="formatted")
   rewind u 
   read(u,*) nat 

   allocate( coord_one(nat) )
   coord_one(:) = ""
   allocate( coord_two(nat) )
   coord_two(:) = ""

   do i = 1, nat 
      read (u, '(a)') line 
      coord_one(i) = line
      coord_two(i) = trim(line)//"     "//"0     0     0        0       0	&
                                 &0	0	0"
   end do
   close(u)

   allocate( vecnumb(nat) )
   vecnumb(:) = 0
   allocate( veclab(nat) ) 
   veclab(:) = ""
   allocate( atype(nat) ) 
   atype(:) = 0
   allocate( lnk(nat,8) )
   lnk(:,:) = 0

   do i = 1, nat 
      read(coord_two(i),*) vecnumb(i), veclab(i), rbuff, rbuff, rbuff, &
                           & atype(i), (lnk(i,j), j = 1,8) 
   end do

   !
   ! => Reads the current geometry from mopac
   !
   allocate( coord(nat,3) )
   coord(:,:) = 0._dpr

   open(newunit=u, file='geom', status='old', form='formatted')
   rewind u
   do i = 1, nat
      read(u,*) cbuff, rbuff, (coord(i,j), j = 1,3), rbuff
   end do
   close(u)
   coord = coord*bohr
  
   !
   ! => Writes the updated geometry tinker's file 
   !
   open(newunit=u, file='fullMM_tnk.xyz', status='replace', form='formatted')
   rewind u  
   write(u,'(3x, i6)') nat
   do i = 1, nat
      write(u,100) vecnumb(i), veclab(i), (coord(i,j), j=1,3), atype(i), &
      (lnk(i,j), j = 1,8)
   end do
   close(u)

   100 format (4x,i8,2x,a4,4x,f20.5,3x,f20.5,3x,f20.5,2x,i8,*(3x,i8))

end program nx2tnk
