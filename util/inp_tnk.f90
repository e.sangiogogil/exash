program inp_tnk

   implicit none

   !********************************************************************
   integer, parameter :: dpr=kind(1.d0)

   integer :: i, j, k, l
   integer :: u 
   integer :: nchrom
   integer :: nel 
   integer :: nat 
   integer :: nlines 
   integer :: io
   integer :: numb
   integer :: nat_last
   integer :: nproc
   integer :: gen_file
   integer :: ibuf
   integer :: nl

   integer, allocatable, dimension(:) :: vecnumb
   integer, allocatable, dimension(:) :: atype
   integer, allocatable, dimension(:) :: atype_new

   integer, allocatable, dimension(:,:) :: atom_group
   integer, allocatable, dimension(:,:) :: lnk
   integer, allocatable, dimension(:,:) :: at_ty

   character(len=200) :: card
   character(len=200) :: name1
   character(len=200) :: name2
   character(len=200) :: line

   character(len=200), allocatable, dimension(:) :: name_mopac
   character(len=200), allocatable, dimension(:) :: coord_one
   character(len=200), allocatable, dimension(:) :: coord_two
   character(len=200), allocatable, dimension(:) :: veclab
   character(len=200), allocatable, dimension(:) :: save_key
   character(len=200), allocatable, dimension(:) :: groups

   real(dpr), allocatable, dimension(:,:) :: geom

   logical :: conferma
   !********************************************************************

   namelist /exc_inp/ nchrom, nproc, gen_file
   open(newunit=u, file='exc.inp', action='read', status='old')
   read(u, nml=exc_inp)
   close(u)

   ! --Reads the "list_exc" file
   open(newunit=u, file='list_exc', form='formatted', status='old')
   rewind u
   read(u,*) nchrom

   allocate( atom_group(nchrom,2) )
   atom_group(:,:) = 0

   do i = 1, nchrom
      read(u,*) (atom_group(i,j), j = 1, 2)
   end do
   nat_last = atom_group(nchrom, 2)
   if (gen_file .ne. 5) then 
      call search("ATOM TYPE", card, u, conferma)
      read(u,*) nel
      allocate( at_ty(nel,2) )
      at_ty(:,:) = 0 
      do i = 1, nel
         read(u,*) (at_ty(i,j), j = 1, 2)
      end do
   end if
   close(u)
   
   ! --Array with the name of the Mopac's files
   allocate( name_mopac(nchrom) )
   name_mopac(:) = ""
   do i = 1, nchrom
      write (name1, "(I10)") i
      write (name2, "(A)") 'exc_mop'
      name_mopac(i) =trim(adjustl(name2))//trim(adjustl(name1))
   end do

   if (gen_file .ne. 5) then
      ! -- 1°: Reads all the rows of the "file_tnk.xyz" file as string
      !        and add "zeros" in the end of each line;
      !
      ! -- 2°: Reads all the coluns of the strings.
      open (newunit=u, file='fullMM_tnk.xyz', status="old", form="formatted")
      rewind u
      read(u,*) nat
      allocate( coord_one(nat), coord_two(nat) )
      coord_one(:) = ""
      coord_two(:) = ""
      do i = 1, nat
         read (u, '(a)') line 
         coord_one(i) = line
         coord_two(i) = trim(line)//"     "//"0 0 0 0 0 0 0 0"
      end do
      close(u)

      allocate( vecnumb(nat) ) 
      vecnumb(:) = 0
      allocate( veclab(nat) )
      veclab(:) = ""
      allocate( geom(nat,3) )
      geom(:,:) = 0._dpr
      allocate( atype(nat) )
      atype(:) = 0
      allocate( lnk(nat,8) )
      lnk(:,:) = 0

      do i = 1, nat
         read(coord_two(i),*) vecnumb(i), veclab(i), (geom(i,j), j = 1,3), &
                             & atype(i), (lnk(i,j), j = 1,8)
      end do
   
      ! --Loop over the chromophores 
      !  - Switch the atoms type
      do i = 1, nchrom
   
         allocate( atype_new(nat) )
         atype_new(:) = 0
   
         atype_new = atype
   
         do j = atom_group(i,1), atom_group(i,2) 
            do k = 1, nel
               if (atype(j) .eq. at_ty(k,2)) then
                  atype_new(j) = at_ty(k,1)
               end if
            end do
         end do
         
         open(newunit=u, file=trim(name_mopac(i))//'_tnk.xyz', status='unknown', &
              & form='formatted')
         rewind u
         write(u,*) nat 
         do j = 1, nat
            write(u,102) vecnumb(j), veclab(j), (geom(j,k), k = 1,3), &
                         & atype_new(j), (lnk(j,l), l = 1, 8)
         end do
         close(u)
   
         deallocate( atype_new )
   
      end do
 
   end if

   if (gen_file .eq. 3 .or. gen_file .eq. 5 .or. gen_file .eq. 1) then
      !
      ! => Generate the tinker.key files
      !   
   
      ! --Checks the number of lines 
      nlines = 0
      open(newunit=u, file='fullMM_tnk.key')
      do 
         read(u,*,iostat=io)
         if (io /= 0) exit
         nlines = nlines + 1
      end do
      close(u)
   
      ! --Read all the lines layout lines 
      allocate( save_key(nlines) ) 
      save_key(:) = ""
      open(newunit=u, file='fullMM_tnk.key', status='old', form='formatted')
      rewind u
      do i = 1, nlines
         read(u,'(a)') save_key(i)
      end do
      close(u)
      
      ! --Loop over the chromophores
      do i = 1, nchrom
         conferma = .false.
         open(newunit=u, file='list_exc', form='formatted', status='old')
         rewind u
         call search("GROUPS", card, u, conferma)
         if (conferma) then
            if (i .eq. 1) then
               read(u,*) ibuf, nl
               allocate( groups(nl) )
               do j = 1, nl
                  read(u,'(a)') groups(j)
               end do
            else
               do j = 1, i-1
                  read(u,*) ibuf, nl
                  do k = 1, nl
                     read(u,*) 
                  end do
               end do
               read(u,*) ibuf, nl
               allocate( groups(nl) )
               do j = 1, nl
                  read(u,'(a)') groups(j)
               end do
            end if
         end if
         close(u)
   
         open(newunit=u, file=trim(name_mopac(i))//'_tnk.key', status='unknown', &
              form='formatted')
         rewind u
    
         do j = 1, nlines
            write(u,'(a)') save_key(j)
         end do
         write(u,*)
         write(u,'(a19)') '# Partial Structure'
   
         if (conferma) then
            do j = 1, nl
               write(u,'(a)') groups(j)
            end do
            deallocate( groups )
         else
            if (i .eq. 1) then     
               numb = atom_group(i,1)*(-1)
               write(u,100) 'GROUP', 1, numb, atom_group(i,2) 
               numb = (atom_group(i,2) + 1)*(-1)
               write(u,100) 'GROUP', 2, numb, nat
               write(u,101) 'GROUP-SELECT', 1, 1, 0.0
               write(u,101) 'GROUP-SELECT', 1, 2, 1.0
               write(u,101) 'GROUP-SELECT', 2, 2, 1.0
            else
               if ( i .eq. nchrom) then
                  if (nat_last .ne. nat) then
                     numb = atom_group(i,1) - 1
                     write(u,100) 'GROUP', 1, -1, numb 
                     numb = atom_group(i,1)*(-1)
                     write(u,100) 'GROUP', 2, numb, atom_group(i,2) 
                     numb = (atom_group(i,2) + 1)*(-1)
                     write(u,100) 'GROUP', 3, numb, nat
                     write(u,101) 'GROUP-SELECT', 1, 1, 1.0
                     write(u,101) 'GROUP-SELECT', 1, 2, 1.0
                     write(u,101) 'GROUP-SELECT', 2, 2, 0.0
                     write(u,101) 'GROUP-SELECT', 1, 3, 1.0
                     write(u,101) 'GROUP-SELECT', 2, 3, 1.0
                     write(u,101) 'GROUP-SELECT', 3, 3, 1.0
                  else
                     numb = atom_group(1,1)*(-1)
                     write(u,100) 'GROUP', 1, numb, (atom_group(i,1)-1) 
                     numb = (atom_group(i,1))*(-1)
                     write(u,100) 'GROUP', 2, numb, nat
                     write(u,101) 'GROUP-SELECT', 1, 1, 1.0
                     write(u,101) 'GROUP-SELECT', 1, 2, 1.0
                     write(u,101) 'GROUP-SELECT', 2, 2, 0.0
                  end if
               else
                  numb = atom_group(i,1) - 1
                  write(u,100) 'GROUP', 1, -1, numb 
                  numb = atom_group(i,1)*(-1)
                  write(u,100) 'GROUP', 2, numb, atom_group(i,2) 
                  numb = (atom_group(i,2) + 1)*(-1)
                  write(u,100) 'GROUP', 3, numb, nat
                  write(u,101) 'GROUP-SELECT', 1, 1, 1.0
                  write(u,101) 'GROUP-SELECT', 1, 2, 1.0
                  write(u,101) 'GROUP-SELECT', 2, 2, 0.0
                  write(u,101) 'GROUP-SELECT', 1, 3, 1.0
                  write(u,101) 'GROUP-SELECT', 2, 3, 1.0
                  write(u,101) 'GROUP-SELECT', 3, 3, 1.0
               end if
            end if
         end if
         close(u)
      end do
   end if

   100 format (a5, 8x, i2, 1x, i10, 1x, i10)
   101 format (a12, 1x, i2, 1x, i2, 2x, f3.1)
   102 format (4x,i2,2x,a2,4x,f8.5,3x,f8.5,3x,f8.5,2x,i6,*(3x,i3))

contains

subroutine search(targt, stringa, nfiles, conferma)
   implicit none
   character (len=*), intent (in)  :: targt
   character (len=*), intent (out) :: stringa
   integer, intent(in)  :: nfiles
   logical, intent(out) :: conferma

   integer :: icod

   intrinsic Index

   conferma=.false.
   do
      if (conferma) exit
      read(nfiles,'(A)',iostat=icod)stringa
      if (icod < 0) exit
      conferma=(Index(stringa,targt) /= 0)
   end do

   return
end subroutine search
    
end program inp_tnk
