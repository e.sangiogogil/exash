#!/bin/bash

#............................................................
# => This scrip checks if the trajectories contain errors.  .
#............................................................

#########INPUT########################
# Number of trajectories to be checked:
ntraj=319
######################################

ini=1
while [ $ini -le $ntraj ]; do
	cd TRAJ${ini}
	if [ -d "DEBUG" ]; then
		cd DEBUG
		err1=$(grep -n forrtl runnx.error | wc -l) 
		err2=$(grep -n STOP runnx.error | wc -l) 
		if [ $err1 -gt 0 ]; then
			echo "The TRAJ${ini} has $err1 errors"
		fi
		if [ $err2 -gt 0 ]; then
			echo "The TRAJ${ini} has $err2 errors"
		fi
		cd ..
	fi
	cd ..
	let ini=ini+1
done
