#!/bin/bash

#..........................................................................
# =>This script copies all the "file.dyn" to a single directory in order  .
# to be posteriorly merged. The trajectories which contains errors won't  .
# be copied.                                                              .
# This subroutine must be executed at directory: TRAJECTORIES             .
#..........................................................................

# =>Input section:.........................................................
# Name of the dyn file, for example: "ex_prog.dyn"
jobname=add_info
# Directory path where the files will be copied:
merge_path=/home/duda/merge_test
# Number of trajectories:
ntraj=303
#..........................................................................

i=1
while [ $i -le $ntraj ]; do
       if [ -d "TRAJ${i}" ]; then
	       cd TRAJ${i}
	       if [ -d "DEBUG" ]; then
		       cd DEBUG
		       err1=$(grep -n forrtl runnx.error | wc -l)
		       err2=$(grep -n STOP runnx.error | wc -l)
		       if [ $err1 -gt 0 ]; then
			       echo "The TRAJ${i} didn't finish well... This trajectory will be discarded. :(" 
			       cd ..
		       elif [ $err2 -gt 0 ]; then
			       echo "The TRAJ${i} didn't finish well... This trajectory will be discarded. :(" 
			       cd ..
		       else 
			       cd ..
			       if [ -d "TEMP" ]; then
				       cd TEMP
				       if [ -f "${jobname}.dyn" ]; then
					       cp ${jobname}.dyn ${jobname}_${i}.dyn &
					       pid=$!
					       wait $pid
					       cp ${jobname}_${i}.dyn $merge_path
				       fi
				       cd ..
			       fi
		       fi
	       fi
	       cd ..
       fi
       let i=i+1
done
       




