#!/bin/bash

#************************************************************************************#
# => This script converts the initial condition in "MOPAC format" into the           #
# initial conditions in "Newton-X format"                                            #
#                                                                                    #
# => For this, the files: "sh.inp", "exc.inp" and the diretory "JOB_NAD" must be     #
# copied at the directory which contains the initial coditions files of mopac.       #
#                                                                                    #
# => A NX geom file must be present too. This script only need this file in          #
# order to know the atom's positions. This file must have the name: geom_model.      #
#                                                                                    #
# => For the use of stop conditions, the file "stopsign.inp" also must be present.   #
#************************************************************************************#

# => INPUT SECTION: ..................................................................
# Number of trajectories:
ntraj=318
# Input name:
name_init_cond=add_info
# Number of atoms:
numat=58
# Total number of states:
nstat=3
# Time step:
dt=0.1
# Time:
ti=2500
# Kill trajectory if total energy deviate more than "e_jump" eV in one time step
e_jump=0.5
# Kill trajectory if total energy deviate more than e_drift (eV) in comparison to the value in t = 0.
e_drift=0.5
#.....................................................................................

mkdir TRAJECTORIES

i=1

while [ $i -le $ntraj ]; do

	istat=$(awk '/ISTAT=/{print $6}' ${name_init_cond}.inicond.${i} | awk '{if (NR==1) print}')
	x=$(awk '{if (NR !=1 && NR !=2) print $1}' ${name_init_cond}.inicond.${i})
	y=$(awk '{if (NR !=1 && NR !=2) print $2}' ${name_init_cond}.inicond.${i})
	z=$(awk '{if (NR !=1 && NR !=2) print $3}' ${name_init_cond}.inicond.${i})
	vx=$(awk '{if (NR !=1 && NR !=2) print $4}' ${name_init_cond}.inicond.${i})
	vy=$(awk '{if (NR !=1 && NR !=2) print $5}' ${name_init_cond}.inicond.${i})
	vz=$(awk '{if (NR !=1 && NR !=2) print $6}' ${name_init_cond}.inicond.${i})
	a1=$(awk '{print $1}' geom_model)
	a2=$(awk '{print $2}' geom_model)
	a3=$(awk '{print $6}' geom_model)

	cd TRAJECTORIES
	
	mkdir TRAJ${i}
	
	cd ..
	
	cp exc.inp TRAJECTORIES/TRAJ${i}
	cp sh.inp TRAJECTORIES/TRAJ${i}
	cp -rf JOB_NAD TRAJECTORIES/TRAJ${i}
	
	if test -f "stopsign.inp"; then
		cp stopsign.inp TRAJECTORIES/TRAJ${i}
	fi
	
	cd TRAJECTORIES
	
	cd TRAJ${i}

cat <<EOF > control.dyn
&input
 nat = $numat
 nstat = $nstat
 nstatdyn = $istat
 dt = $dt
 tmax = $ti
 prog = 4.5
 thres = 100
 killstat = 1
 timekill = 0
 ndamp = 0
 lvprt = 2
 kt = 1
 Etot_jump = $e_jump
 Etot_drift = $e_drift
/
EOF

	paste <(printf "%s\n" "$a1") <(printf "%s\n" "$a2") <(printf "%s\n" "$x") <(printf "%s\n" "$y") <(printf "%s\n" "$z") <(printf "%s\n" "$a3") > geom

	paste <(printf "%s\n" "$vx") <(printf "%s\n" "$vy") <(printf "%s\n" "$vz") > veloc

	cd ../..

	let i=i+1
done



