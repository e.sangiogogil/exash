#!/bin/bash

#.............................................
# => Run many trajectories in the same time  .
#.............................................

##INPUT####
#Fisrt trajectory:
ini=31
#Last trajactory:
fim=40
##########

while [ $ini -le $fim ]; do
       cd TRAJ${ini}
       $NX/moldyn.pl > moldyn.log &
       cd ..
       let ini=ini+1 
done

