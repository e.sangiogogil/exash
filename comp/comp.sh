#!/bin/bash

#INPUTS (Defaults):
libs="\$(LAPACKLIB)"
fc="gfortran"
ld="gfortran -v"
static=""
fflags="-cpp -Wall -pedantic -g -fcheck=all -Og -DDEBUG"
ldflags="\$(FFLAGS) \$(LIBS)"
lapacklib="-llapack"
make=make
#

cat <<EOF > make.template
LIBS      = $libs
FC        = $fc
LD        = $ld
STATIC    = $static
FFLAGS    = $fflags
LDFLAGS   = $ldflags
LAPACKLIB = $lapacklib
EOF

perl ./mkmf.perl -v -p ../bin/exash -t ./make.template -m ./Makefile ../src &
pid=$!
wait $pid

$make 

cd ../util

$make


