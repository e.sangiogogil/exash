program exash

!**************************************************************************!                              
! => This program calculates:                                              !  
!    * The Excition Energies in the Excition Model;                        !  
!    * The Gradients for the Exciton Model;                                !  
!    * The wave function overlaps between two consecutive time steps.      !  
!                                                                          !  
! -> The excitonic coupling between two states on different chromophores   !  
!    are performed using the transition monopole aproximation (TMA)        !   
!    which is based on the transition charges (TrESP).                     !  
!                                                                          !  
! -> The diagonal terms of the Exciton Hamiltonian are taken from the      !  
!    the FOMO-CI excitation energies.                                      !
!                                                                          !  
!**************************************************************************!                                                                            

   use dyn_mod, only: dyn_option
   use sp_mod, only: sp_option

   implicit none

   !***********************************************************************
   integer, parameter :: dpr = kind(1.d0)
   integer :: nchrom
   !! Number of chromophores
   integer :: nstat_tot
   !! Total number excited states
   integer :: nat_tot
   !! Number os atoms of all the chromophores
   integer :: tresp
   !! Use the Transition Monopole approximation if "tresp=1"
   integer :: freeze
   !! Freeze atons if "freeze=1"
   integer :: ifree
   !! First atom to be freezed
   integer :: ffree
   !! Last atom to be freezed
   integer :: verbose
   !! If "verbose=1", it will generate an output
   integer :: dip
   !! Option to calculate the tran. dip. moments and osc. strenght
   integer :: sp 
   !! Option to perform a single point calculation (default = 0)
   integer :: numb_atom
   !! Total number of atoms in the system
   integer :: isurf
   !! The state root
   integer :: gs
   !! Option to calculate just the gradients and energies of the ground state
   integer :: i
   integer :: u

   integer, allocatable, dimension(:) :: nstat
   !! Array which contains the number of states of each chrom.
   integer, allocatable, dimension(:) :: nat_array
   !! Array which contains the number of atoms of each chrom.

   character(len=120) :: name_file
   !! General name of the mopac files
   character(len=120) :: out_name
   !! Name for the output files genarate by this program
   character(len=120) :: coup_file
   !! Name of the file which contains the electronic coup.
   
   character(len=120) :: name_ind, file_esp
   
   character(len=120), allocatable, dimension(:) :: name_mopac

   real(dpr) :: start, finish

   !! Array with the name of the mopac files for all the chrom.
   !***********************************************************************

   !
   ! => Input informations:
   !
   namelist /DAT/ name_file, nchrom, out_name, dip, sp, gs

   call cpu_time(start)
   !
   ! => Default input informations:
   !
   tresp = 1    !* Off dig terms of the H will be performed using TMA aproximation.
   freeze = 0   !* All the atoms can move.
   ifree = 0
   ffree = 0
   coup_file = ""
   name_file = "exc_mop"
   out_name = "exc_mop"
   nchrom = 2
   dip = 0      ! if dip > 0, calculates the tran. dip. moments and oscillator strenght
   sp = 0       ! Default is to use the dyn mode
   gs = 0       ! Default is to compute the excited state properties
   isurf = 1    ! Default is the ground state
   !
   !
   !
   open(newunit=u, file='excinp', action='read', status='old')
   read(u, nml=DAT)
   close(u)
   !read(5,DAT)
   !
   allocate( nat_array(nchrom) )
   nat_array(:) = 0
   allocate( nstat(nchrom) )
   nstat = 0
   allocate( name_mopac(nchrom) )
   name_mopac(:) = ""
    
   numb_atom = 0 
   !
   if (sp .eq. 0) then
      call namelist_dyn(tresp, verbose, nstat, nat_array, &
                        & freeze, ifree, ffree)
   else
      call namelist_sp(tresp, numb_atom, isurf, nstat, nat_array)
   end if

   if (sp .ne. 0 .and. numb_atom .eq. 0) then
      write(*,*) "ERROR: the number of atoms (numb_atom) must be provided in &
                  &the SP namelist (excinp file)" 
      call exit
   end if

   if (tresp .eq. 1) then
      continue
   else
      call namelist_coup(coup_file,len(coup_file))
   end if
   !
   nstat_tot = sum(nstat)
   nat_tot = sum(nat_array)

   if (gs .gt. 0) then
      nstat_tot = 0
   end if

   !
   ! => Array which contains the name of the Mopac's files
   !
   do i = 1, nchrom
      write (name_ind, "(I10)") i
      write (file_esp, "(A)") 'exc_mop'
      name_mopac(i) = trim(adjustl(file_esp))//trim(adjustl(name_ind))
   end do

   if (sp .eq. 0) then
      call dyn_option(tresp, nstat, nat_array, name_mopac, out_name, &
                     & coup_file, nstat_tot, nat_tot, nchrom, freeze, &
                     & ifree, ffree, verbose, dip, gs)
   else
      continue
      call sp_option(tresp, numb_atom, isurf, nstat, nat_array, &
                    & name_mopac, out_name, coup_file, nstat_tot, & 
                    & nat_tot, nchrom, dip, gs)
   end if

   call cpu_time(finish)
   print '("Time = ",f6.3," seconds.")',finish-start
contains
 
 !
 ! => Input for dynamical calculation
 !
 subroutine namelist_dyn(tresp, verbose, nstat, nat_array, &
                         & freeze, ifree, ffree)
    implicit none

    integer, parameter :: dpr=kind(1.d0)

    integer :: tresp
    integer :: freeze
    integer :: ifree
    integer :: ffree
    integer :: verbose

    integer, dimension(:), intent(inout) :: nstat
    integer, dimension(:), intent(inout) :: nat_array

    namelist /DYN/ tresp, verbose, nstat, nat_array, freeze, &
                   & ifree, ffree

    open(newunit=u, file='excinp', action='read', status='old')
    read(u, nml=DYN)
    close(u)
 end subroutine namelist_dyn

 !
 ! => Input single point calculation
 !
 subroutine namelist_sp(tresp, numb_atom, isurf, nstat, nat_array)
    implicit none
    
    integer, parameter :: dpr=kind(1.d0)

    integer :: tresp
    integer :: numb_atom
    integer :: isurf

    integer, dimension(:), intent(inout) :: nstat
    integer, dimension(:), intent(inout) :: nat_array

    namelist /SP/ tresp, numb_atom, isurf, nstat, nat_array

    open(newunit=u, file='excinp', action='read', status='old')
    read(u, nml=SP)
    close(u)
 end subroutine namelist_sp

 !
 ! => Input for coup
 !
 subroutine namelist_coup(coup_file,ll)
    implicit none
    integer, intent(in) :: ll
    character(len=ll), intent(inout) :: coup_file

    namelist /COUP/ coup_file
    open(newunit=u, file='excinp', action='read', status='old')
    read(u, nml=COUP)
    close(u)
    !read(5,COUP)
 end subroutine namelist_coup

end program exash
