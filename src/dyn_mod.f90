module dyn_mod

   use en_grad_mod, only: calc_en_grad
   use ovl_mod, only: calc_ovl

   implicit none
   private
  
   public :: dyn_option
   public :: nx_interface
   public :: wout

 contains
!********************************************************************************
subroutine dyn_option(tresp, nstat, nat_array, name_mopac, out_name, &
                      & coup_file, nstat_tot, nat_tot, nchrom, freeze, &
                      & ifree, ffree, verbose, dip, gs)

   integer, parameter :: dpr = kind(1.d0)

   integer :: i, j, k
   integer :: u
   integer :: ind1, ind2, ind_tot
   integer :: ibuf
   integer :: nchrom
   integer :: nstat_tot
   integer :: nat_tot
   integer :: isurf
   integer :: nsurf
   integer :: numb_atom
   integer :: nc
   integer :: tresp
   integer :: freeze
   integer :: ifree 
   integer :: ffree
   integer :: verbose
   integer :: nnx 
   integer :: nstat_t
   integer :: dip
   integer :: gs

   integer, dimension(:), intent(inout) :: nstat
   integer, dimension(:), intent(inout) :: nat_array
   
   integer, allocatable, dimension(:) :: indice1, indice2
   
   integer, allocatable, dimension(:,:) :: stati_trdip

   real(dpr), parameter :: eV=27.211386245988

   real(dpr) :: time
   real(dpr) :: ti
   real(dpr) :: rbuf

   real(dpr), dimension(:), allocatable :: vec_en
   real(dpr), dimension(:), allocatable :: osc_st
   real(dpr), dimension(:), allocatable :: mat_ene
   real(dpr), dimension(:), allocatable :: pop_dia

   real(dpr), dimension(:,:), allocatable :: ham_tot
   real(dpr), dimension(:,:), allocatable :: eigenvectors
   real(dpr), dimension(:,:), allocatable :: velo
   real(dpr), dimension(:,:), allocatable :: grad_total
   real(dpr), dimension(:,:), allocatable :: trdip_debye
   real(dpr), dimension(:,:), allocatable :: mat_overlap
   real(dpr), dimension(:,:), allocatable :: adoverlap
   real(dpr), dimension(:,:), allocatable :: eci
   real(dpr), dimension(:,:), allocatable :: grad_allx, grad_ally, grad_allz
   real(dpr), dimension(:,:), allocatable :: coordinates

   real(dpr), dimension(:,:,:), allocatable :: ovl_tot
   real(dpr), dimension(:,:,:), allocatable :: trdip_tot
   real(dpr), dimension(:,:,:), allocatable :: tr_q

   character(len=120) :: out_name
   character(len=120) :: coup_file 
   character(len=120) :: cbuf

   character(len=120), dimension(:), intent(inout) :: name_mopac
!********************************************************************************

   if (gs .gt. 1) then
      isurf = 1
      dip = 0
   end if

   !
   ! => Extract the dyn informations from Newton-X
   !
   call nx_interface(numb_atom, nc, nsurf, isurf, time, ti)
   
   if (nc .eq. 0 .and. nsurf .ne. (nstat_tot+1) ) then
      write(*,*)
      write(*,*) "The number of states in Newton-X is: ", nsurf
      write(*,*) "The number of states in Exciton program is: ", (nstat_tot+1)
      write(*,*) "The number of states in Newton-X and in exciton &
                  &program must be the same..."
      write(*,*) "Aborting calculation..."
      call exit
      write(*,*)
   end if

   allocate( vec_en(nstat_tot+1) )
   vec_en(:) = 0._dpr
   allocate( osc_st(nstat_tot) )
   osc_st(:) = 0._dpr
   allocate( mat_ene(nstat_tot) )
   mat_ene(:) = 0._dpr
   allocate( ham_tot(nstat_tot+1, nstat_tot+1) )
   ham_tot(:,:) = 0._dpr
   allocate( eigenvectors(nstat_tot+1, nstat_tot+1) )
   eigenvectors(:,:) = 0._dpr
   allocate( grad_total(numb_atom,3) )
   grad_total(:,:) = 0._dpr
   allocate( trdip_debye(nstat_tot, 3) )
   trdip_debye(:,:) = 0._dpr

   !
   ! => Read the "mopac.exash" and save all the CI energies, gradients, 
   !    transition dipoles and ovl mat
   !
   allocate(eci((nstat_tot+1),nchrom))
   eci(:,:) = 0._dpr
   allocate(grad_allx((nstat_tot*numb_atom),nchrom))
   grad_allx(:,:) = 0._dpr
   allocate(grad_ally((nstat_tot*numb_atom),nchrom))
   grad_ally(:,:) = 0._dpr
   allocate(grad_allz((nstat_tot*numb_atom),nchrom))
   grad_allz(:,:) = 0._dpr
   allocate(ovl_tot(nstat_tot,nstat_tot,nchrom))
   ovl_tot(:,:,:) = 0._dpr
   k = nstat_tot*(nstat_tot+1)/2
   allocate(trdip_tot(k,nchrom,3))
   trdip_tot(:,:,:) = 0._dpr
   allocate(stati_trdip(k,2))
   stati_trdip(:,:) = 0
   allocate(tr_q(nat_tot,nstat_tot,nchrom))
   tr_q(:,:,:) = 0._dpr

   ind1 = 0
   ind2 = 0
   do i = 1, nchrom
      open(newunit=u, file=Trim(Adjustl(name_mopac(i)))//'_nx.exash', &
           &status='old', form='formatted')
      rewind u
      read(u,*) ind1
      do j = 1, ind1
         read(u,*) eci(j,i)
      end do
      read(u,*) ind1, ind2
      ind_tot = ind1*ind2
      do j = 1, ind_tot
         read(u,*) grad_allx(j,i), grad_ally(j,i), grad_allz(j,i)
      end do
      read(u,*) ind1
      do j = 1, ind1
         read(u,*) (ovl_tot(j,k,i), k=1,ind1)
      end do
      read(u,*) ind1
      do j = 1, ind1
         read(u,*) stati_trdip(j,1), stati_trdip(j,2), trdip_tot(j,i,1), &
                   & trdip_tot(j,i,2), trdip_tot(j,i,3)
      end do
      read(u,*) ind1, ind2
      do j = 1, ind1
         read(u,*) ibuf, cbuf, (tr_q(j,k,i), k=1,ind2)
      end do
      close(u)
   end do
   ! CHECK THE OVERLAP MATRIX
   !open(newunit=u, file="check_ovl", status='unknown')
   !rewind u
   !do i = 1, nchrom
   !write(u,*) "chromophore:", i
   !   do j = 1, (nstat(i)+1)
   !      write(u,*) (ovl_tot(j,k,i), k = 1, (nstat(i)+1))
   !   end do
   !end do
   !close(u)

   !
   ! => Read geometry
   !   
   allocate(coordinates(numb_atom,3))
   coordinates(:,:) = 0._dpr
   open(newunit=u, file="geom", status='old', form='formatted')
   rewind u
   do i = 1, numb_atom
      read(u,*) cbuf, rbuf, (coordinates(i,j), j=1,3), rbuf
   end do
   close(u)

   !
   ! => Call a subroutine which computes the energies, gradients and trans. dip.
   !    moments
   !
   call calc_en_grad(numb_atom, isurf, tresp, nstat, nat_array, name_mopac, &
                     & nstat_tot, nat_tot, nchrom, coup_file, ham_tot, &
                     & vec_en, eigenvectors, grad_total, trdip_debye, osc_st, & 
                     & mat_ene, dip, gs, eci, grad_allx, grad_ally, grad_allz, &
                     & trdip_tot, tr_q, coordinates, stati_trdip)

   !
   ! => Write epot file
   !
   open(newunit=u, file="epot", status='unknown', form='formatted')
   do i = 1, nstat_tot+1
      write(u,102) vec_en(i)
   end do 
   close(u)

   !
   ! => If one wants to consider freeze atoms, the program must rewrite the 
   ! velocities by writing the vel. of the freeze atoms to be 0 in order to 
   ! avoid numerical problems... Also we need to set the gradients of those
   ! atoms 0.
   !
   if ( freeze .eq. 1 ) then
      allocate( velo(numb_atom, 3) )

      !-> Set the gradients as 0 for the freeze atoms:
      do i = ifree, ffree
         do j = 1, 3
            grad_total(i,j) = 0._dpr
         end do
      end do

      !-> Rewrite the velocities in order to avoid numerical problems
      open(newunit=u, file='veloc', status='unknown', form='formatted')
      rewind u
      do i = 1, numb_atom
         read(u,*) (velo(i,j), j = 1, 3)
      end do
      close(u)

      do i = ifree, ffree
         do j = 1, 3
            velo(i,j) = 0._dpr
         end do
      end do
      
      open(newunit=u, file='veloc', status='unknown', form='formatted')
      rewind u
      do i = 1, numb_atom
         write(u,*) (velo(i,j), j = 1, 3)
      end do
      close(u)
   end if
         
   !
   ! => Write the gradients in a external file
   !
   open (newunit=u, file='grad', status='replace', form='formatted')           
   do i = 1, numb_atom
      write(u,100) ( grad_total(i,j), j = 1, 3)
   end do
   close(u)

   !
   ! => Write a "fake" grad.all
   !
   open (newunit=u, file='grad.all', status='replace', form='formatted')           
   do i = 1, nstat_tot+1
      do j = 1, numb_atom
         write(u,100) ( grad_total(j,k), k = 1, 3)
     end do
   end do
   close(u)

   !
   ! => Call a subroutine which computes the overlap matrix
   !
   allocate( adoverlap(nstat_tot+1, nstat_tot+1), &
             mat_overlap(nstat_tot+1, nstat_tot+1) )
   adoverlap(:,:) = 0._dpr
   mat_overlap(:,:) = 0._dpr
    
   if (gs .gt. 0) then
      adoverlap(1,1) = 1._dpr
      open(newunit=u, file='run_cioverlap.log', status='unknown', form='formatted')
      rewind u
      write(u,*)
      write(u,*) adoverlap(1,1)
      close(u)
   else
      call calc_ovl(nchrom, nstat, mat_overlap, adoverlap, &
                   & nstat_tot, nc, eigenvectors, ovl_tot)
   end if
   
   !
   ! => Fake: writes zeros at the place of the nonadiabatic couplings
   !
   open (newunit=u,file='nad_vectors',form='formatted',status='unknown')
   rewind u
   nstat_t = nstat_tot + 1
   nnx = (nstat_t*nstat_t - nstat_t) / 2
   do i = 1, nnx
      do j = 1, numb_atom
         write(u,*) 0._dpr, 0._dpr, 0._dpr
      end do
   end do
   close(u)

   !
   ! => Replace the eigenvectors of the previous step into eigenvectors of 
   ! the current step
   !
   open(newunit=u, file="old_eigenvectors.txt", status='replace', &
        & form='formatted')
   rewind u
   do i = 1, nstat_tot+1
      write(u,*) (eigenvectors(i,j), j = 1, nstat_tot+1)
   end do
   close(u)

   !
   ! => Computes the diabatic populations
   !
   allocate( pop_dia(nstat_tot+1) )
   pop_dia(:) = 0._dpr
   do i = 1, nstat_tot + 1
      pop_dia(i) = ( eigenvectors(i, isurf) ) ** 2
   end do
   
   !
   ! => Write the diabatic hamiltonian and the diabatic populations
   !
   open(newunit=u, file='../RESULTS/exc.inf', status='unknown', position='append', &
        & action='write')
   write(u,*) "Time step:", nc
   write(u,*) 'Diabatic Hamiltonian:'
   write(u,*) (nstat_tot + 1)
   do i = 1, nstat_tot + 1
      write(u,*) (ham_tot(i,j), j = 1, nstat_tot + 1)
   end do
   write(u,*) 'Diabatic populations:'
   write(u,*) (nstat_tot + 1)
   do i = 1, nstat_tot + 1
      write(u,*) pop_dia(i)
   end do
   write(u,*) '-----------------------------------------------------------------------------'
   write(u,*)
   close(u)
   
   ! 
   ! => Write the oscillator strenght at the RESULTS file
   ! 
   if (dip .gt. 0) then
      allocate( indice1(nstat_tot), indice2(nstat_tot) )
      indice1(:) = 0
      indice2(:) = 0

      do i = 1, nstat_tot
         indice1(i) = i + 1
      end do
      do i = 1, nstat_tot
         indice2(i) = 1
      end do

      open(newunit=u, file='../RESULTS/osc_exc', status='unknown', position='append', &
           & action='write')

      write(u,*) "Time step:", nc
      write(u,*)
      write(u,*) "  States          Excitation Energy (eV)       Oscillator strength (a.u.)"
      write(u,*)  
      do i = 1, nstat_tot
         write(u,'(2x,i3,i3,9x,f20.15,11x,f20.15)') indice2(i), indice1(i), &
                                                    & (mat_ene(i)*eV), osc_st(i)
      end do
      write(u,*) '-----------------------------------------------------------------------------'
      write(u,*)
      close(u)
      deallocate( indice1, indice2 )
   end if

   !
   ! => Writes the Trans. dip. moments 
   !
   open(newunit=u, file='trdip_exc', status='unknown')
   rewind u
   do i = 1, nstat_tot
      write(u,*) (trdip_debye(i,j), j = 1, 3)
   end do
   close(u)

   !
   ! => Writes the exciton hamiltonian
   !
   open(newunit=u, file='ham_exc', status='unknown')
   rewind u
   do i = 1, nstat_tot+1
      write(u,*) (ham_tot(i,j), j = 1, nstat_tot+1)
   end do
   close(u)
   
   !
   ! => In case of "verbose = true" generate a formatted file
   ! 
   if (verbose .eq. 1) then
      call wout(out_name, nc, tresp, ti, numb_atom, nsurf, isurf, &
                & time, nstat_tot, ham_tot, mat_ene, eigenvectors, &
                & mat_overlap, adoverlap, trdip_debye, osc_st, dip)
   end if

100 format (2x, *(g12.5, 4x))
102 format (2x, *(f20.9, 4x))
end subroutine dyn_option

!
! => Read informations from control.d
!
subroutine nx_interface(natom, nc, nsurf, isurf, time, ti)
   integer, parameter :: dpr=kind(1.d0)
   integer :: natom, nc, nsurf, isurf, idum, u
   real(dpr) :: time, ti

   !
   ! => Opens and reads file control.d
   !
   open (newunit=u, file='control.d', form='formatted', status='old')
   rewind u
   read(u,*) natom, nc, nsurf, isurf, idum, idum, time, ti
   close(u)
end subroutine nx_interface

!
! => Formatted output
!

subroutine wout(out_name, nc, tresp, ti, numb_atom, nsurf, isurf, &
                & time, nstat_tot, ham_tot, mat_ene, eigenvectors, &
                & mat_overlap, adoverlap, trdip_debye, osc_st, dip)
!********************************************************************************
   integer, parameter :: dpr=kind(1.d0)

   integer :: i, j
   integer :: u
   integer :: nc
   integer :: numb_atom
   integer :: nsurf
   integer :: isurf
   integer :: nstat_tot
   integer :: tresp
   integer :: dip

   integer, allocatable, dimension(:) :: indice1, indice2

   real(dpr), parameter :: eV=27.211386245988

   real(dpr) :: ti
   real(dpr) :: time

   real(dpr), dimension(:), intent(inout) :: mat_ene
   real(dpr), dimension(:), intent(inout) :: osc_st

   real(dpr), dimension(:,:), intent(inout) :: adoverlap
   real(dpr), dimension(:,:), intent(inout) :: mat_overlap
   real(dpr), dimension(:,:), intent(inout) :: ham_tot
   real(dpr), dimension(:,:), intent(inout) :: eigenvectors
   real(dpr), dimension(:,:), intent(inout) :: trdip_debye

   character(len=120) :: out_name
!********************************************************************************

   open(newunit=u, file='../RESULTS/'//trim(out_name)//'.out', &
        & status='unknown', position='append', action='write')
   if (nc == 0) then
      write(u,*)
      write(u,*) "***************************************************************************"
      write(u,*) "*                       _______  __ ___   _____ __  __                    *"
      write(u,*) "*                      / ____/ |/ //   | / ___// / / /                    *"
      write(u,*) "*                     / __/  |   // /_|| \__ \/ /_/ /                     *" 
      write(u,*) "*                    / /___ /   |/ ___ |___/ / __  /                      *" 
      write(u,*) "*                   /_____//_/|_/_/  |_/____/_/ /_/                       *" 
      write(u,*) "*                                                                         *" 
      write(u,*) "*           EXcitonic Analysis for Surface Hopping dynamics               *" 
      write(u,*) "*                                                                         *" 
      write(u,*) "*                             Eduarda S. Gil                              *" 
      write(u,*) "*                             November 2021                               *" 
      write(u,*) "*                                                                         *" 
      write(u,*) "*                                                                         *" 
      write(u,*) "* => This program calculates:                                             *"
      write(u,*) "*    -> The excitation energies within the exciton model;                 *"
      write(u,*) "*    -> Approximated electronic couplings;                                *"
      write(u,*) "*    -> The gradients for the exciton model;                              *" 
      write(u,*) "*    -> The wave function overlaps between two consecutive time steps     *"
      write(u,*) "*       (it's needed in the local diabatization algorithm);               *"
      write(u,*) "*    -> The transition dipole Moments for the exciton Model;              *" 
      write(u,*) "*    -> Diabatic populations                                              *" 
      write(u,*) "*                                                                         *" 
      write(u,*) "***************************************************************************"
      if (tresp .eq. 1) then
         write(u,*)
         write(u,*) "The excitonic coupling between two states on different chromophores will be" 
         write(u,*) "evaluated  using the  transition monopole aproximation (TMA) which is based" 
         write(u,*) "on the transition charges (TrESP)."
      else
         write(u,*)
         write(u,*) "The excitonic coupling between two states on different chromophores will be" 
         write(u,*) "evaluated exactly." 
      end if
      write(u,*)
      write(u,*)
   end if 

   write(u,*)
   write(u,*)"--------------------------------------------------------------------"
   write(u,*)
   write(u,*)" STEP:", nc
   write(u,*)" TIME:", ti
   write(u,*)
   write(u,*)"--------------------------------------------------------------------"
   write(u,*)

   write(u,*)"*********** => Informations read from file control.d <= ************"
   write(u,*)
   write(u,'(a,i8)') " natom =", numb_atom
   write(u,'(a,i8)') " nc    =", nc
   write(u,'(a,i8)') " nsurf =", nsurf
   write(u,'(a,i8)') " isurf =", isurf
   write(u,'(a,F20.9)') " timestep   =", time
   write(u,*)
   write(u,*)"********************************************************************"

   !
   ! => Print the full Hamiltonian
   !
   write(u,*)
   write(u,*)
   write(u,*) "=> EXCITONIC HAMILTONIAN (a.u) <="
   write(u,*)
   do i = 1, nstat_tot + 1
      write(u,'(*(f15.8))') (ham_tot(i,j), j = 1, nstat_tot + 1)
   end do

   !
   ! => Write the Excitonic Energies 
   !
   write(u,*)
   write(u,*)
   write(u,*) "=> EXCITONIC ENERGIES (a.u.) <="
   write(u,*)
   do i = 1, nstat_tot 
      write(u,'(2x,f20.9)') mat_ene(i) 
   end do
   write(u,*)

   !
   ! => Print the eigenvectors
   !
   write(u,*)
   write(u,*) " => EIGENVECTORS OF THE EXCITONIC HAMILTONIAN (a.u.) <= "
   do i = 1, nstat_tot+1
      write(u,100) (eigenvectors(i,j), j = 1,nstat_tot+1)
   end do
   write(u,*)

   !
   ! => Print the overlap matrix in diabatic basis
   !
   write(u,*)
   write(u,*) " => OVERLAP MATRIX IN DIABATIC BASIS <= "
   do i = 1, nstat_tot+1
      write(u,'(1x,*(1x, f16.8))') (mat_overlap(i,j), j = 1, nstat_tot+1)
   end do
   write(u,*)

100 format (2x, *(g12.5, 4x))
   !
   ! => Print the overlap matrix in adiabatic basis
   !
   write(u,*)
   write(u,*) " => OVERLAP MATRIX IN ADIABATIC BASIS <= "
   do i = 1, nstat_tot+1
      write(u,'(1x,*(1x, f16.8))') (adoverlap(i,j), j = 1, nstat_tot+1)
   end do
   write(u,*)

   !
   ! => Print the trdip
   !
   if (dip .gt. 0) then
      allocate( indice1(nstat_tot), indice2(nstat_tot) )
      indice1(:) = 0
      indice2(:) = 0

      do i = 1, nstat_tot
         indice1(i) = i + 1
      end do
      do i = 1, nstat_tot
         indice2(i) = 1
      end do

      write(u,*)
      write(u,*) "                            => Transition dipoles in Debye <="
      write(u,*) "   I  J                                <I| mu |J>"
      do i = 1, nstat_tot
         write(u,'(2x,i3,i3,5x,*(f20.15))') indice2(i), indice1(i), &
                                            & (trdip_debye(i,j), j=1,3)
      end do
      write(u,*) 
      write(u,*) 

      !
      ! => Write the oscillator strenght
      !
      mat_ene = mat_ene*eV
      write(u,*) "  States          Excitation Energy(eV)          Oscillator strength"
      write(u,*)  
      do i = 1, nstat_tot
         write(u,'(2x,i3,i3,9x,f20.15,11x,f20.15)') indice2(i), indice1(i), &
                                                    & mat_ene(i), osc_st(i)
      end do
      write(u,*)
   end if

   close(u)

end subroutine wout

end module dyn_mod
