module en_grad_mod

   implicit none
   private

   public :: calc_en_grad
   public :: emm
   public :: ene
   public :: rxyz
   public :: ext_coup
   public :: Eigfunc
   public :: grad_S0
   public :: rgrad
   public :: off_dig_grad
   public :: trandip
   public :: cerca

 contains
!*****************************************************************
 
subroutine calc_en_grad(numb_atom, isurf, tresp, nstat, nat_array, name_mopac, &
                        & nstat_tot, nat_tot, nchrom, coup_file, ham_tot, &
                        & vec_en, eigenvectors, grad_total, trdip_debye, osc_st, &
                        & mat_ene, dip, gs, eci, grad_allx, grad_ally, grad_allz, &                        
                        & trdip_tot, tr_q, coordinates, stati_trdip)

!*****************************************************************
   integer, parameter :: dpr = kind(1.d0)
   integer :: i, j, k, l, g, h,  m, n, p, q, w, z
   integer :: u
   integer :: nchrom
   integer :: nstat_tot
   integer :: nat_tot
   integer :: isurf
   integer :: numb_atom
   integer :: unz, init1, init2
   integer :: ii, jj, zz, yy, iii, jjj
   integer :: ngrad
   integer :: num_lab1, num_lab2 
   integer :: ind_coup 
   integer :: num_grad 
   integer :: ind_test
   integer :: atom_dif 
   integer :: tresp
   integer :: dip
   integer :: gs

   integer, dimension(:), intent(inout) :: nstat
   integer, dimension(:), intent(inout) :: nat_array

   integer, dimension(:,:), intent(inout) :: stati_trdip

   real(dpr), parameter :: debye=2.541747760
   real(dpr), parameter :: kcal=627.509

   real(dpr) :: enMM
   real(dpr) :: e_gs
   real(dpr) :: e_ref
   real(dpr) :: eint
   real(dpr) :: r
   real(dpr) :: num_x, num_y, num_z
   real(dpr) :: full_grd_x, full_grd_y, full_grd_z
   real(dpr) :: osc 
   real(dpr) :: mat_en 
   real(dpr) :: mod_trd

   real(dpr), dimension(:), intent(inout) :: vec_en
   real(dpr), dimension(:), intent(inout) :: osc_st
   real(dpr), dimension(:), intent(inout) :: mat_ene

   real(dpr), dimension(:,:), intent(inout) :: ham_tot
   real(dpr), dimension(:,:), intent(inout) :: eigenvectors
   real(dpr), dimension(:,:), intent(inout) :: grad_total
   real(dpr), dimension(:,:), intent(inout) :: trdip_debye
   real(dpr), dimension(:,:), intent(inout) :: eci
   real(dpr), dimension(:,:), intent(inout) :: grad_allx, grad_ally, grad_allz
   real(dpr), dimension(:,:), intent(inout) :: coordinates
   
   real(dpr), dimension(:,:,:), intent(inout) :: trdip_tot
   real(dpr), dimension(:,:,:), intent(inout) :: tr_q

   real(dpr), allocatable, dimension(:) :: omega
   real(dpr), allocatable, dimension(:) :: array_gs
   real(dpr), allocatable, dimension(:) :: grad_off_dig_x, grad_off_dig_y, &
                                           & grad_off_dig_z 
   real(dpr), allocatable, dimension(:) :: elec_coup
   real(dpr), allocatable, dimension(:) :: eig_vec
   real(dpr), allocatable, dimension(:) :: full_grd_x_prima, full_grd_y_prima, &
                                           & full_grd_z_prima
   real(dpr), allocatable, dimension(:) :: mod_trdip

   real(dpr), allocatable, dimension(:,:) :: mat_e_ref
   real(dpr), allocatable, dimension(:,:) :: coord1, coord2
   real(dpr), allocatable, dimension(:,:) :: qtot_1, qtot_2
   real(dpr), allocatable, dimension(:,:) :: ham_exp 
   real(dpr), allocatable, dimension(:,:) :: mat_ene2
   real(dpr), allocatable, dimension(:,:) :: grad_GS
   real(dpr), allocatable, dimension(:,:) :: grad_dig
   real(dpr), allocatable, dimension(:,:) :: grad_dig_tot
   real(dpr), allocatable, dimension(:,:) :: grad_off
   real(dpr), allocatable, dimension(:,:) :: full_grad_mat
   real(dpr), allocatable, dimension(:,:) :: grad1, grad2, grad3
   real(dpr), allocatable, dimension(:,:) :: trdip
   real(dpr), allocatable, dimension(:,:) :: ham

   character(len=120) :: coup_file
   character(len=120) :: card
   character(len=120) :: file_charge1, file_charge2
   character(len=120) :: lab1, lab2

   character(len=120), dimension(:), intent(inout) :: name_mopac

   logical :: conferma
!*****************************************************************
   allocate( ham(nstat_tot, nstat_tot) )
   ham(:,:) = 0._dpr
   allocate( mat_e_ref(nstat_tot+1, nstat_tot+1) )
   mat_e_ref(:,:) = 0._dpr
   allocate( mat_ene2(nstat_tot, nstat_tot) ) 
   mat_ene2(:,:) = 0._dpr
   allocate( ham_exp(nstat_tot+1, nstat_tot+1) )
   ham_exp(:,:) = 0._dpr
   allocate( grad1(nstat_tot, nstat_tot), grad2(nstat_tot, nstat_tot), &
             & grad3(nstat_tot, nstat_tot) )
   grad1(:,:) = 0._dpr
   grad2(:,:) = 0._dpr
   grad3(:,:) = 0._dpr

   !
   ! => Extract the MM energy from Tinker output
   !
   call emm(enMM) 
   enMM = enMM / kcal

   !
   ! => Deal with the energies
   !
   allocate( omega(nstat_tot) )
   omega(:) = 0._dpr
   allocate( array_gs(nchrom) )
   array_gs(:) = 0._dpr
   do i = 1, nchrom
      array_gs(i) = eci(1,i)
      do j = 2, nstat(i)+1
         k = k + 1
         omega(k) = eci(j,i) - eci(1,i)
      end do
   end do
         
   !g = 0
   !h = 0 
   !do i = 1, nchrom
   !  h = h + 1
   !   call ene(name_mopac(i), nstat(i), g, omega, array_gs, h)
   !end do
   
   !
   ! => Computes the reference energy
   !
   e_gs = 0._dpr

   do i = 1, nchrom
      e_gs = e_gs + ( array_gs(i) - enMM )
   end do

   e_ref = enMM + e_gs 
  
   do i = 1, nstat_tot + 1
      mat_e_ref(i,i) = e_ref
   end do

   if (gs .gt. 0) then
      vec_en(1) = e_ref
   else
      !
      ! => Computes the diagonal terms of the Exciton Hamiltonian
      !
      do i = 1, nstat_tot
         ham (i,i) = omega(i)
      end do
   
      if (tresp .eq. 1) then
         !
         ! => Computes the off-diagonal terms of the Exciton Hamiltonian (electronic couplings) 
         ! and the off-diagonal dH/dR derivatives for the case of TrESP aproximation...                                              
         !
      
         ! -> Array's dimension:
         ngrad = 0
         do i = 1, nchrom
            q = nat_array(i)*nstat(i)
            do j = 1, nchrom
               if (j .ne. i) then
                  ngrad = ngrad + q*nstat(j)
               end if
            end do
         end do
   
         allocate( grad_off_dig_x(ngrad), grad_off_dig_y(ngrad), grad_off_dig_z(ngrad) )
         grad_off_dig_x(:) = 0._dpr
         grad_off_dig_y(:) = 0._dpr
         grad_off_dig_z(:) = 0._dpr
   
         w = 1 
         ! -> Loop over the chromophores
         do m = 1, nchrom  
            init1 = 0
            do iii = 1, m-1
               init1 = init1 + nat_array(iii)
            end do
         
            do n = 1, nchrom
               init2 = 0
               do jjj = 1, n-1
                  init2 = init2 + nat_array(jjj)
               end do
   
               if (m .ne. n) then
               
                  allocate( coord1(nat_array(m),3), coord2(nat_array(n),3) )
                  coord1(:,:) = 0._dpr
                  coord2(:,:) = 0._dpr
                  allocate( qtot_1(nat_array(m),nstat(m)), qtot_2(nat_array(n),nstat(n)) )
                  qtot_1(:,:) = 0._dpr
                  qtot_2(:,:) = 0._dpr

                  !! Deal with the geometries
                  j = 0
                  do i = (init1+1), (init1+1+nat_array(m))
                     j = j + 1 
                     coord1(j,1) = coordinates(i,1)
                     coord1(j,2) = coordinates(i,2)
                     coord1(j,3) = coordinates(i,3)
                  end do
                  j = 0
                  do i = (init2+1), (init2+1+nat_array(n))
                     j = j + 1 
                     coord2(j,1) = coordinates(i,1)
                     coord2(j,2) = coordinates(i,2)
                     coord2(j,3) = coordinates(i,3)
                  end do

                  !! Deal with the charges
                  do i = 1, nat_array(m)
                     do j = 1, nstat(m)
                        qtot_1(i,j) = tr_q(i,j,m)
                     end do
                  end do
                  do i = 1, nat_array(n)
                     do j = 1, nstat(n)
                        qtot_2(i,j) = tr_q(i,j,n)
                     end do
                  end do
   
                  !call rxyz(nat_array(m), coord1, init1)
                  !call rxyz(nat_array(n), coord2, init2)
   
                  !file_charge1 = name_mopac(m)
                  !file_charge2 = name_mopac(n)
   
                  !open(newunit=u, file=trim(file_charge1)//'.out', status='old', &
                  !     & form='formatted')
                  !rewind u
                  !call cerca("MATRIX WITH THE ELECTROSTATIC CHARGES OF ALL THE STATES", &
                  !           & card, u, conferma)
                  !do i = 1, nat_array(m)
                  !   read(u,*) num_lab1, lab1, ( qtot_1(i,j), j = 1,nstat(m) )
                  !end do
                  !close(u)
   
                  !open(newunit=u, file=trim(file_charge2)//'.out', status='old', &
                  !     & form='formatted')
                  !rewind u
                  !call cerca("MATRIX WITH THE ELECTROSTATIC CHARGES OF ALL THE STATES", &
                  !           & card, u, conferma)
                  !do i = 1, nat_array(n)
                  !  read(u,*) num_lab2, lab2, ( qtot_2(i,j), j = 1,nstat(n) )
                  !end do
                  !close(u)
   
                  ! -> Loop over the states
                  ii = 0
                  do p = 1, m - 1 
                     ii = ii + nstat(p)
                  end do
   
                  do i = 1, nstat(m) 
               
                     ii = ii + 1   
   
                     jj = 0
                     do q = 1, n - 1
                        jj = jj + nstat(q)
                     end do
   
                     do j = 1, nstat(n) 
                        jj = jj + 1
                        eint = 0._dpr
                        ! => Loop over the atoms
                        do k = 1, nat_array(m) 
                           num_x = 0._dpr
                           num_y = 0._dpr
                           num_z = 0._dpr
   
                           do l = 1, nat_array(n)
   
                              r = sum((coord1(k,1:3) - coord2(l,1:3))**2)
                           
                              ! -> Calculate the transition monopole aproximation (TMA) using
                              !    the transition charges (TrESP).       
                              eint = eint + qtot_1(k,i) * qtot_2(l,j) / sqrt(r)
   
                              ! -> Calculate the dH/dR derivatives       
                              num_x = num_x - (((qtot_1(k,i) * qtot_2(l,j)) * (coord1(k,1) &
                                      & - coord2(l,1))) / r**(1.5)) 
                              num_y = num_y - (((qtot_1(k,i) * qtot_2(l,j)) * (coord1(k,2) &
                                      & - coord2(l,2))) / r**(1.5)) 
                              num_z = num_z - (((qtot_1(k,i) * qtot_2(l,j)) * (coord1(k,3) &
                                      & - coord2(l,3))) / r**(1.5)) 
                           end do
   
                              grad_off_dig_x(w) = num_x
                              grad_off_dig_y(w) = num_y
                              grad_off_dig_z(w) = num_z
                         
                              w = w + 1
                        end do
                        ham(ii,jj) = eint
                     end do
                  end do
                  deallocate( coord1, coord2 ) 
                  deallocate( qtot_1, qtot_2 )
               end if
            end do
         end do
      else
         !
         ! => Compute the off-diagonal terms using mopac -> ab initio way to calculate...
         !    * For now, this approach works only in the case of two chromophores!
         !
         allocate( elec_coup(nstat(1)*nstat(2)) )
         elec_coup(:) = 0._dpr
         call ext_coup(nstat, elec_coup, coup_file)
   
         ind_coup = 0   
   
         do m = 1, nchrom-1
            ii = 0
            do p = 1, m - 1 
               ii = ii + nstat(p)
            end do
            do n = m+1, nchrom
               ii = 0
               do i = 1, nstat(m)
                  ii = ii + 1
                  jj = 0
                  do q = 1, n - 1 
                     jj = jj + nstat(q)
                  end do
                  do j = 1, nstat(n)
                     jj = jj + 1
                     ind_coup = ind_coup + 1
                     ham(ii,jj) = elec_coup(ind_coup) 
                     ham(jj,ii) = ham(ii,jj)  
                  end do
               end do
            end do
         end do
   
      end if
   
      !
      ! => Add the GS energy (reference) in the Exciton Hamiltonian in 
      ! order to obtain the full Hamiltonian
      !
      do i = 2, nstat_tot + 1
         do j = 2, nstat_tot + 1
            ham_exp(i,j) = ham(i-1, j-1)
         end do
      end do
   
      do i = 1, nstat_tot + 1
         do j = 1, nstat_tot + 1
            ham_tot(i,j) = ham_exp(i,j) + mat_e_ref(i,j)
         end do
      end do
   
      !
      ! => Diagonalize the Hamiltonian in order to obtain the eigenvalues (energies)  
      ! and eigenvectors (unitary matrix).                                            
      !
      call Eigfunc(ham, nstat_tot, mat_ene) 
    
      do i = 1, nstat_tot 
         mat_ene2(i,i) = mat_ene(i)
      end do
   
      vec_en(1) = e_ref
      do i = 2, nstat_tot+1
         vec_en(i) = mat_ene(i-1) + e_ref
      end do
   
      !
      ! => Construct the unitary matrix including the ground state
      !
      eigenvectors(1,1) = 1._dpr
      do i = 2, nstat_tot + 1
         do j = 2, nstat_tot + 1
            eigenvectors(i,j) = ham(i-1, j-1)
         end do
      end do

   end if
   !
   ! => Gradients section INIT
   !
   allocate ( grad_GS(numb_atom,3) )
   grad_GS(:,:) = 0._dpr

   call grad_S0(numb_atom, nchrom, grad_GS, grad_allx, grad_ally, grad_allz)

   if (isurf .gt. 1) then
         
      ! -> Gradients of the site energies:  

      ! * Compute the size of the array with the dig grad
      num_grad = 0
      do i = 1, nchrom
         num_grad = num_grad + numb_atom*nstat(i)
      end do

      allocate ( grad_dig(num_grad, 3) )
      grad_dig(:,:) = 0._dpr
  
      w = 1
      do i = 1, nchrom
         do k = 1, nstat(i)   
            ii = k * numb_atom
         
            allocate( grad_dig_tot(numb_atom*(nstat(i)+1),3) )
            grad_dig_tot(:,:) = 0._dpr

            !! Deal with the gradients
            do j = 1, (nstat(i)+1)*numb_atom
               grad_dig_tot(j,1) = grad_allx(j,i)
               grad_dig_tot(j,2) = grad_ally(j,i)
               grad_dig_tot(j,3) = grad_allz(j,i)
            end do

            !call rgrad(numb_atom, nstat(i), name_mopac(i), grad_dig_tot)
            
            do j = 1, numb_atom
               ii = ii + 1
               grad_dig(w,1) = grad_dig_tot(ii,1) - grad_dig_tot(j,1)  
               grad_dig(w,2) = grad_dig_tot(ii,2) - grad_dig_tot(j,2) 
               grad_dig(w,3) = grad_dig_tot(ii,3) - grad_dig_tot(j,3) 
               w = w + 1
            end do

            deallocate( grad_dig_tot )
         end do
      end do
 
      if (tresp .eq. 1) then
         continue
      else
        ! -> Read the off-dig gradients for the case in which the TrESP 
        ! aproximation is NOT take into account!
        ! * Array's dimension:
         ngrad = 0
         do i = 1, nchrom
            q = nat_array(i) * nstat(i)
            do j = 1, nchrom
               if (j .ne. i) then
                  ngrad = ngrad + q * nstat(j)
               end if
            end do
         end do

         allocate( grad_off(ngrad,3) )
         grad_off(:,:) = 0._dpr
         call off_dig_grad(nchrom, nat_array, nstat, grad_off, coup_file)
       end if 
      
       ! -> Final gradients section: 
       allocate( eig_vec(nstat_tot) )
       eig_vec(:) = 0._dpr

       ! * Eigenvector of the current state
       do i = 1, nstat_tot
          eig_vec(i) = ham(i,(isurf-1))
       end do
   
       allocate ( full_grd_x_prima(nstat_tot), full_grd_y_prima(nstat_tot), &
                 full_grd_z_prima(nstat_tot) ) 
       full_grd_x_prima(:) = 0._dpr
       full_grd_y_prima(:) = 0._dpr
       full_grd_z_prima(:) = 0._dpr
       allocate( full_grad_mat(numb_atom,3) )
       full_grad_mat(:,:) = 0._dpr

       w = 0
       z = 0
       unz = 0
       g = 0
       ind_test = 0
       do i = 1, nchrom
          if (nstat_tot .eq. nchrom) then
             if (i .gt. 1) then
                w = w + nat_array(i-1)*nstat(i-1)
             else
                w = w
             end if
          else
             if (i .gt. 1) then
                w = w + nat_array(i-1)*nstat_tot
             else
                w = w
             end if
          end if

          !if (nstat_tot .eq. 2) then
          !   w = w + nat_array(i-1)*nstat(i-1)
          !else
          !   w = w + nat_array(i-1)*nstat_tot
          !end if
          do j = 1, nat_array(i)
             unz = unz + 1
             grad1 = 0._dpr
             grad2 = 0._dpr
             grad3 = 0._dpr
             z = z + 1
             do k = 1, nchrom-1
        
                do l = k+1, nchrom
                   ii = 0
                   do p = 1, k-1
                      ii = ii + nstat(p)
                   end do
          
                   do m = 1, nstat(k)
                      ii = ii + 1
                      jj = 0
                      do q = 1, l-1
                         jj = jj + nstat(q)
                      end do
                      h = nat_array(i)*nstat(k)*(m-1)
            
                      do n = 1, nstat(l)
                         jj = jj + 1
                         g = h + w + nat_array(i)*(n-1) + j 
                         ind_test =  ind_test + 1
                         if (tresp .eq. 1) then
                            grad1(ii,jj) = grad_off_dig_x(g)
                            grad1(jj,ii) = grad1(ii,jj)
                            grad2(ii,jj) = grad_off_dig_y(g)
                            grad2(jj,ii) = grad2(ii,jj)
                            grad3(ii,jj) = grad_off_dig_z(g)
                            grad3(jj,ii) = grad3(ii,jj)
                         else
                            grad1(ii,jj) = grad_off(ind_test,1)
                            grad1(jj,ii) = grad1(ii,jj)
                            grad2(ii,jj) = grad_off(ind_test,2)
                            grad2(jj,ii) = grad2(ii,jj)
                            grad3(ii,jj) = grad_off(ind_test,3)
                            grad3(jj,ii) = grad3(ii,jj)
                         end if
                      end do
                   end do
                 end do
              end do
              do yy = 1, nstat_tot
                 zz = (yy-1)*numb_atom + unz
                 grad1(yy,yy) = grad_dig(zz,1)
                 grad2(yy,yy) = grad_dig(zz,2)
                 grad3(yy,yy) = grad_dig(zz,3)
              end do
              full_grd_x_prima = matmul(grad1, eig_vec)
              full_grd_x = dot_product(eig_vec, full_grd_x_prima)
              full_grd_y_prima = matmul(grad2, eig_vec)
              full_grd_y = dot_product(eig_vec, full_grd_y_prima)
              full_grd_z_prima = matmul(grad3, eig_vec)
              full_grd_z = dot_product(eig_vec, full_grd_z_prima)
 
              full_grad_mat(z,1) = full_grd_x
              full_grad_mat(z,2) = full_grd_y
              full_grad_mat(z,3) = full_grd_z
            end do
         end do

         atom_dif = numb_atom - nat_tot
   
         if (nat_tot < numb_atom) then
            do j = 1, atom_dif
               z = z + 1
               unz = unz + 1
               grad1 = 0._dpr
               grad2 = 0._dpr
               grad3 = 0._dpr
               do k = 1, nstat_tot
                  zz = (k-1) * numb_atom + unz
                  grad1(k,k) = grad_dig(zz,1)
                  grad2(k,k) = grad_dig(zz,2)
                  grad3(k,k) = grad_dig(zz,3)
               end do
               full_grd_x_prima = matmul(grad1, eig_vec)
               full_grd_x = dot_product(eig_vec, full_grd_x_prima)
               full_grd_y_prima = matmul(grad2, eig_vec)
               full_grd_y = dot_product(eig_vec, full_grd_y_prima)
               full_grd_z_prima = matmul(grad3, eig_vec)
               full_grd_z = dot_product(eig_vec, full_grd_z_prima)

               full_grad_mat(z,1) = full_grd_x
               full_grad_mat(z,2) = full_grd_y
               full_grad_mat(z,3) = full_grd_z
            end do
         end if
      
      ! -> Add to the exciton gradients the GS gradients in order to 
      ! take the total gradients
      do i = 1, numb_atom
         full_grad_mat(i,1) = full_grad_mat(i,1) + grad_GS(i,1)
         full_grad_mat(i,2) = full_grad_mat(i,2) + grad_GS(i,2)
         full_grad_mat(i,3) = full_grad_mat(i,3) + grad_GS(i,3)
      end do
 
      grad_total = full_grad_mat

   else 

      grad_total = grad_GS

   end if
   !
   ! => Gradients sections END   
   !

   !
   ! => Calculate the transition dipole moment
   !
   if ( dip .gt. 0 ) then
      allocate( trdip(nstat_tot, 3) )
      trdip(:,:) = 0._dpr
      call trandip(nchrom, nstat, ham, trdip, trdip_tot, stati_trdip)
      trdip_debye = trdip * debye

      !
      ! => Calculate the oscillator strenght
      !
      allocate ( mod_trdip(nstat_tot) )
      mod_trdip(:) = 0._dpr
  
      !-> Calculate the module of the transitions dipole moments
      do i = 1, nstat_tot
         mod_trdip(i) = sqrt( trdip(i,1)**2 + trdip(i,2)**2 + trdip(i,3)**2 )
      end do

      osc_st = 0._dpr
      do i = 1, nstat_tot
         mat_en = mat_ene(i)
         mod_trd = mod_trdip(i)
         osc = mat_en * (mod_trd**2) * 2.0_dpr / 3.0_dpr
         osc_st(i) = osc
      end do 
   end if

end subroutine calc_en_grad

!
! => Read the total MM energy from a external Tinker file.
!
subroutine emm(enMM)
   integer, parameter :: dpr=kind(1.d0)
   integer :: u
   real(dpr) :: enMM
   logical :: conferma
   character(len=120) :: card

   open(newunit=u, file="fullMM_tnk.out", status='old', form='formatted')
   rewind u 
   call cerca("Total Potential Energy :", card, u, conferma)
   read(card(27:),*)  enMM
   close(u)
end subroutine emm

!
! => Read the QM/MM energies from a external Mopac file.
!
subroutine ene(file_name, nstat, ind, omega, array_gs, ind2)
   integer, parameter :: dpr=kind(1.d0)

   integer :: i
   integer :: u
   integer :: ind
   integer :: ind2
   integer :: nstat

   character(len=120) :: file_name

   real(dpr), dimension(:), intent(inout) :: omega
   real(dpr), dimension(:), intent(inout) :: array_gs

   real(dpr) :: en_au
   real(dpr) :: en_au1

   open(newunit=u, file=trim(file_name)//"_nx.epot", status='old', &
        & form='formatted')
   rewind u
   read(u,*) en_au1
   array_gs(ind2) = en_au1
   do i = 1, nstat
      ind = ind + 1   
      read(u,*) en_au
      omega(ind) = en_au - array_gs(ind2)
   end do
   close(u)
end subroutine ene

 !
 ! => Read the geometries NX format
 !
subroutine rxyz (natn, coordn, init)
   integer, parameter :: dpr=kind(1.d0)
   integer :: natn, init, i, j, u
   real(dpr), dimension(:,:), intent(inout) :: coordn
   character(len=4) :: cbuff
   real(dpr) :: rbuff

   open(newunit=u, file="geom", status='old', form='formatted')
   rewind u
   do i = 1, init
      read(u,*) 
   end do 
   do i = 1, natn
      read(u,*) cbuff, rbuff, (coordn(i,j), j=1,3), rbuff
   end do
   close(u)
end subroutine rxyz

 !
 ! => Extract the electronic couplings
 !
 subroutine ext_coup(nstat, elec_coup, nfile)      
    integer, parameter :: dpr = kind(1.d0)
    integer :: i, stat1, stat2, total, u
    integer, dimension(:), intent(inout) :: nstat
    real(dpr), dimension(:), intent(inout) :: elec_coup
    character(len=120) ::  nfile, card
    logical :: conferma

    total = nstat(1)*nstat(2)
 
    open(newunit=u, file=nfile, status='old', form='formatted')
    rewind u
    call cerca("State_Chrom_1  State_Chrom_2     Exciton coupling (au)", &
               & card, u, conferma)
    do i = 1, total
       read(u,*) stat1, stat2, elec_coup(i)
    end do
    close(u)
 end subroutine ext_coup

!
! => Call a lapack subroutine in order to diagonalize the Excition Hamiltionian
!
subroutine Eigfunc(ham, nstat_tot, mat_ene)

   integer, parameter :: dpr=kind(1.d0)
   integer :: nstat_tot
   real(dpr), dimension(nstat_tot, nstat_tot) :: ham
   real(dpr), dimension(nstat_tot) :: mat_ene 
   real(dpr), allocatable, dimension(:) :: work
   integer :: Lwork, info

   Lwork = max(25,nstat_tot**2)
   allocate(work(lwork))

   ! => Solve eigenproblem
   call DSYEV('V', 'U', nstat_tot, ham, nstat_tot, mat_ene, work, Lwork, info)

   deallocate(work)
   if (INFO > 0) then
       write(*,*)'ERROR: '
       write(*,*)'ERROR: The algorithm failed to compute eigenvalues.'
       write(*,*)''
   end if

end subroutine Eigfunc
!
! => Call a lapack subroutine in order to diagonalize the Excition Hamiltionian
!    (using dsyevd rather than dsyev)
!
subroutine Eigfunc_d(ham, nstat_tot, mat_ene)

   integer, parameter :: dpr=kind(1.d0)
   integer :: nstat_tot, Lwmax
   real(dpr), dimension(nstat_tot, nstat_tot) :: ham
   real(dpr), dimension(nstat_tot) :: mat_ene 
   real(dpr), allocatable, dimension(:) :: work
   integer,   allocatable, dimension(:) :: Iwork
   integer :: Lwork, Liwork, info

   Lwmax = 5000
   Lwork = -1
   Liwork = -1

   allocate(work(1),iwork(1))
   call DSYEVD('V', 'U', nstat_tot, ham, nstat_tot, mat_ene, work, &
               & Lwork, Iwork, Liwork, info)
   LWORK = max( 1, nint( WORK( 1 ) ) )
   LIWORK = max( 1, IWORK(1) )
   deallocate(work,iwork)
   
   write(6,*) ' In EXASH: lwork, liwork:', lwork,liwork,nstat_tot
   allocate(work(lwork),iwork(liwork))

   ! => Solve eigenproblem
   call DSYEVD('V', 'U', nstat_tot, ham, nstat_tot, mat_ene, work, &
              Lwork, Iwork, Liwork, info)

   deallocate(work,iwork)
   if (INFO > 0) then
       write(*,*)'ERROR: '
       write(*,*)'ERROR: The algorithm failed to compute eigenvalues.'
       write(*,*)''
   end if

end subroutine Eigfunc_d

!
! => The following subroutine computes the ground state gradients: 
!
subroutine grad_S0(nat, nchrom, grad, grad_allx, grad_ally, grad_allz)
   integer, parameter :: dpr=kind(1.d0)
   integer :: i, j, nat, nchrom, ibuff, u
   character(len=120) :: nfile, card, cbuff
   real(dpr), parameter :: kcal=627.509, bohr=0.529177210903
   real(dpr) :: grad_gs_x, grad_gs_y, grad_gs_z
   real(dpr), dimension(:,:), intent(inout) :: grad_allx, grad_ally, grad_allz
   real(dpr), allocatable, dimension(:,:) :: grad_x, grad_y, grad_z, &
                                             & tnkgrd, grad
   logical :: conferma

   allocate( grad_x(nat,nchrom), grad_y(nat,nchrom), grad_z(nat,nchrom) )
   grad_x(:,:) = 0._dpr
   grad_y(:,:) = 0._dpr
   grad_z(:,:) = 0._dpr
   allocate( tnkgrd(nat,3) )
   tnkgrd(:,:) = 0._dpr 

   !
   ! => Reads the QM/MM gradients for the ground state
   !
   do i = 1, nchrom
      do j = 1, nat
         grad_x(j,i) = grad_allx(j,i)
         grad_y(j,i) = grad_ally(j,i)
         grad_z(j,i) = grad_allz(j,i)
      end do
   end do
   !do i = 1, nchrom
   !nfile = name_file(i)
   !   open(newunit=u, file=trim(nfile)//"_nx.grad.all", status='old', &
   !        & form='formatted')
   !   rewind u
   !   do j = 1, nat
   !      read(u,*) grad_x(j,i), grad_y(j,i), grad_z(j,i)
   !   end do
   !   close(u)
   !end do

   !
   ! => Reads the MM gradients 
   !
   open(newunit=u, file="fullMM_tnk.out", status='old', form='formatted')
   rewind u 
   call cerca("Cartesian Gradient Breakdown over Individual Atoms :", &
              & card, u, conferma)
   read(u,*)
   read(u,*)
   read(u,*)
   do i = 1, nat
      read(u,*) cbuff, ibuff, (tnkgrd(i,j), j = 1,3)
   end do
   close(u)

   tnkgrd = (tnkgrd/kcal)* bohr

   !
   ! => Calculates the ground state gradients
   !
   do i = 1, nat
      grad_gs_x = 0
      grad_gs_y = 0
      grad_gs_z = 0

     !do j = 1, nchrom
     !   grad_gs_x = grad_gs_x + ( grad_x(i,j) - tnkgrd(i,1) )
     !   grad_gs_y = grad_gs_y + ( grad_y(i,j) - tnkgrd(i,2) )
     !   grad_gs_z = grad_gs_z + ( grad_z(i,j) - tnkgrd(i,3) )
     !end do
     do j = 1, nchrom
        grad_gs_x = grad_gs_x + grad_x(i,j)
        grad_gs_y = grad_gs_y + grad_y(i,j)
        grad_gs_z = grad_gs_z + grad_z(i,j) 
     end do

      !grad(i,1) = tnkgrd(i,1) + grad_gs_x
      !grad(i,2) = tnkgrd(i,2) + grad_gs_y
      !grad(i,3) = tnkgrd(i,3) + grad_gs_z
      grad(i,1) = grad_gs_x
      grad(i,2) = grad_gs_y
      grad(i,3) = grad_gs_z
   end do

   grad = grad - ((nchrom-1)*tnkgrd)
  
   deallocate( grad_x, grad_y, grad_z )
   deallocate( tnkgrd )
end subroutine grad_S0

!
! => Read the gradients from MOPAC 
!
subroutine rgrad(nat, nstat, name_grad, grad_dig_tot)
   integer, parameter :: dpr=kind(1.d0)
   integer :: i, j, nstat, nat, u
   character(len=120) :: name_grad
   real(dpr), dimension(:,:), intent(inout) :: grad_dig_tot

   open(newunit=u, file=trim(name_grad)//"_nx.grad.all", &
        & status='old', form='formatted')
   rewind u
   do i = 1, (nstat+1)*nat
      read(u,*) (grad_dig_tot(i,j), j = 1,3)
   end do
   close(u)
end subroutine rgrad
 
!
! => Read the off-diagonal gradients
! 
subroutine off_dig_grad(nchrom, nat_array, nstat, grad_off, nfile)
   integer, parameter :: dpr=kind(1.d0)
   integer :: nchrom, i, j, k, l, m, n, total, stat1, stat2, u
   integer, dimension(:), intent(inout) :: nat_array, nstat
   real(dpr) :: grad
   real(dpr), dimension(:,:), intent(inout) :: grad_off
   character(len=120) ::  nfile, card
   logical :: conferma

   total = nstat(1) * nstat(2)

   open(newunit=u, file=nfile, status='old', form='formatted')
   rewind u
   call cerca("State_Chrom_1  State_Chrom_2     Non-relaxed derivative of V_ec (au) wrt coord.     1", &
               & card, u, conferma)
   m = 0
   n = 0
   do i = 1, nchrom
      do j = 1, nat_array(i)
         do l = 1, 3
            m = total*(j-1) + n
            do k = 1, total
               m = m + 1
               read(u,*) stat1, stat2, grad
               grad_off(m,l) = grad
            end do
         read(u,*)
         end do
      end do
      n = m
   end do
   close(u)
end subroutine off_dig_grad

!
! => Calculate the transition dipole moments
!
subroutine trandip(nchrom, nstat, ham, trdip, trdip_tot, stati_trdip)
   integer, parameter :: dpr=kind(1.d0)
   integer :: nchrom, i, ii, j, jj, stati, statj, k, nstat_tot, u
   integer, dimension(:), intent(inout) :: nstat
   integer, dimension(:,:), intent(inout) :: stati_trdip
   real(dpr) :: compx, compy, compz, dipx, dipy, dipz
   real(dpr), allocatable,  dimension(:,:) :: mu
   real(dpr), allocatable,  dimension(:) :: eigvec
   real(dpr), dimension(:,:), intent(inout) :: ham, trdip
   real(dpr), dimension(:,:,:), intent(inout) :: trdip_tot
   !
   !
   nstat_tot = sum(nstat)
   allocate( mu(nstat_tot,3) )
   mu(:,:) = 0._dpr

   k = 0
   do i = 1, nchrom
      ! -> Calculate the dimension of the next loop
      jj = 0
      do ii = 1, (nstat(i) + 1)
         jj = jj + ii
      end do
    
      do j = 1, jj
         if (stati_trdip(j,1) .ne. 1 .and. stati_trdip(j,2) .eq. 1) then
            k = k + 1
            mu(k,1) = trdip_tot(j,i,1)
            mu(k,2) = trdip_tot(j,i,2)
            mu(k,3) = trdip_tot(j,i,3)
         end if
      end do

      !filename = trim(name_mopac(i))//".out"
      !open(newunit=u, file=filename, status='old', action='read', &
      !  & form='formatted')
      !rewind u
      !call cerca("Dipoles (au)", card, u, conferma)
      !read(u,*)
      !do j = 1, jj
      !   read(u,*) stati, statj, compx, compy, compz
      !   if (stati .ne. 1 .and. statj .eq. 1) then
      !      k = k + 1
      !      mu(k,1) = compx
      !      mu(k,2) = compy
      !      mu(k,3) = compz
      !   end if
      !end do
      !close(u)
   end do

  do ii = 1, nstat_tot
     allocate( eigvec(nstat_tot) )
     eigvec(:) = 0._dpr
     ! => Eigenvector of the current state
     eigvec = 0._dpr
     do i = 1, nstat_tot
        eigvec(i) = ham(i,ii)
     end do
     dipx = 0._dpr
     dipy = 0._dpr
     dipz = 0._dpr
     do k = 1, nstat_tot
        dipx = dipx + eigvec(k)*mu(k,1)
        dipy = dipy + eigvec(k)*mu(k,2)
        dipz = dipz + eigvec(k)*mu(k,3)
     end do
     trdip(ii,1) = dipx
     trdip(ii,2) = dipy
     trdip(ii,3) = dipz
     deallocate( eigvec )
  end do
end subroutine trandip

subroutine cerca(targt,stringa,nfiles,conferma)
   character (len=*), intent (in)  :: targt
   character (len=*), intent (out) :: stringa
   integer, intent(in)  :: nfiles
   logical, intent(out) :: conferma

   integer :: icod

   intrinsic Index

   conferma=.false.
   do
      if (conferma) exit
      read(nfiles,'(A)',iostat=icod)stringa
      if (icod < 0) exit
      conferma=(Index(stringa,targt) /= 0)
   end do

   return
end subroutine cerca

end module en_grad_mod
