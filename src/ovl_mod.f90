module ovl_mod

   implicit none
   private
 
   public :: calc_ovl
   !public :: roverlap
   public :: adia_overlap

 contains
!*****************************************************************
subroutine calc_ovl(nchrom, nstat, mat_overlap, adoverlap, &
                & nstat_tot, nc, eigenvectors, ovl_tot)

   integer, parameter :: dpr = kind(1.d0)
   
   integer :: i, j, k, l, h, m, n, p, q, w, y, z
   integer :: ii, jj
   integer :: nc
   integer :: nchrom
   integer :: nstat_tot
   integer :: u

   integer, dimension(:), intent(inout) :: nstat
   
   character(len=120) :: filename

   real(dpr) :: ovl_gs
   real(dpr) :: res
   real(dpr) :: rand

   real(dpr), allocatable, dimension(:) :: ovlgs
   real(dpr), allocatable, dimension(:) :: firstline
   real(dpr), allocatable, dimension(:) :: prod
   real(dpr), allocatable, dimension(:) :: firstarray

   real(dpr), allocatable, dimension(:,:) :: temp_over
   real(dpr), allocatable, dimension(:,:) :: temp_over1
   real(dpr), allocatable, dimension(:,:) :: temp_over2

   real(dpr), dimension(:,:), intent(inout) :: mat_overlap
   real(dpr), dimension(:,:), intent(inout) :: adoverlap
   real(dpr), dimension(:,:), intent(inout) :: eigenvectors

   real(dpr), dimension(:,:,:), intent(inout) :: ovl_tot
!*****************************************************************

   !
   ! -> First element of the overlap matrix <S0|S0>
   !
   allocate( ovlgs(nchrom) )
   ovlgs(:) = 0._dpr

   !do i = 1, nchrom
      !filename = name_mopac(i)      
      !open(newunit=u, file=trim(filename)//'_nx.run_cioverlap.log', &
      !     & status='old', form='formatted')
      !rewind u
      !read(u,*)
      !read(u,*) ovl_gs
      !close(u)
   !end do
   
   do i = 1, nchrom
      ovlgs(i) = ovl_tot(1,1,i)
   end do

   mat_overlap(1,1) = product(ovlgs)

   !
   ! -> Build the first line of the overlap matrix
   !
   w = 1
   do i = 1, nchrom
      !filename = name_mopac(i)      
      !open(newunit=u, file=trim(filename)//'_nx.run_cioverlap.log', &
      !     & status='old', form='formatted')
      !allocate ( firstline(nstat(i)+1) )
      !firstline(:) = 0._dpr
      !rewind u
      !read(u,*)
      !read(u,*) ( firstline(j), j = 1, (nstat(i)+1) )
      !close(u)

      allocate( firstline(nstat(i)+1) )
      firstline(:) = 0._dpr
      do j = 1, (nstat(i)+1)
         firstline(j) = ovl_tot(1,j,i)
      end do

      allocate ( prod(nchrom) )
      prod(:) = 0._dpr
      h = 0
      prod = 0._dpr
      do l = 1, nchrom
         if (l .ne. i ) then
            prod(l) = ovlgs(l)
         else
            prod(l) = 1._dpr
         end if
      end do
      res = product(prod)

      do k = 2, nstat(i)+1
         w = w + 1
         mat_overlap(1,w) = firstline(k) * res
      end do
      deallocate ( prod, firstline )
   end do

   !
   ! -> Build the first array of the overlap matrix
   !
   w = 1
   do i = 1, nchrom
      !filename = name_mopac(i)      
      !open(newunit=u, file=trim(filename)//'_nx.run_cioverlap.log', &
      !     & status='old', form='formatted')
      !allocate ( firstarray(nstat(i)+1) )
      !firstarray(:) = 0._dpr
      !rewind u
      !read(u,*)
      !do j = 1, nstat(i)+1
      !   read(u,*) rand
      !   firstarray(j) = rand
      !end do
      !close(u)

      allocate( firstarray(nstat(i)+1) )
      firstarray(:) = 0._dpr
      do j = 1, (nstat(i)+1)
         firstarray(j) = ovl_tot(j,1,i)
      end do

      allocate ( prod(nchrom) )
      do k = 1, nstat(i)
         w = w + 1
         prod = 0._dpr
         res =0._dpr
         h = 0
         do l = 1, nchrom
            if (l .ne. i ) then
               prod(l) = ovlgs(l)
            else
               prod(l) = 1._dpr
            end if
         end do
         res = product(prod)
         mat_overlap(w,1) = firstarray(k+1)*res
      end do
      deallocate(prod) 
      deallocate(firstarray) 
   end do

   ! 
   ! -> Build the diagonal blocks
   !
   y = 1
   do i = 1, nchrom

      y = y + nstat(i-1)

      ! Take the overlap matrix for chromophore "i"
      allocate ( temp_over(nstat(i)+1, nstat(i)+1) )
      temp_over(:,:) = 0._dpr

      !call roverlap(temp_over, nstat(i), name_mopac(i))

      do j = 1, (nstat(i)+1)
         do k = 1, (nstat(i)+1)
            temp_over(j,k) = ovl_tot(j,k,i)
         end do
      end do
  
      ! Take the product overlap of the other chromophores (for this case is only between GS state)
      allocate ( prod (nchrom-1) )
      prod(:) = 0._dpr

      h = 0    
      q = 0 
      do l = 1, nchrom
         if (l .ne. i ) then
            q = q + 1
            prod(q) = ovlgs(l)
         end if
      end do
      res = product(prod)
      deallocate ( prod ) 
    
      do j = 1, nstat(i)
         z = y + j

         do k = 1, nstat(i)
            w = y + k
            mat_overlap(z,w) = temp_over(j+1,k+1)*res
         end do
      end do
      deallocate( temp_over )
   end do

   !
   ! -> Build the off-diagonal blocks
   !
   ! Loop over the chromophores:
   do m = 1, nchrom
      do n = 1, nchrom
         if (m .ne. n) then 
            allocate ( prod (nchrom-2) )
            prod(:) = 0._dpr

            prod = 0._dpr
            h = 0
            q = 0
            do l = 1, nchrom
               if (l .ne. m .and. l .ne. n ) then
                  q = q + 1
                  prod(q) = ovlgs(l)
               end if
            end do
            res = product(prod)
            deallocate ( prod )
     
            if (nchrom .le. 2) then
               res = 1._dpr
            end if 
      
            allocate ( temp_over1((nstat(m)+1),(nstat(m)+1)), &
                       & temp_over2((nstat(n)+1),(nstat(n)+1)) )
            temp_over1(:,:) = 0._dpr
            temp_over2(:,:) = 0._dpr

            do i = 1, (nstat(m)+1)
               do j = 1, (nstat(m)+1)
                  temp_over1(i,j) = ovl_tot(i,j,m)
                end do
            end do
            do i = 1, (nstat(n)+1)
               do j = 1, (nstat(n)+1)
                  temp_over2(i,j) = ovl_tot(i,j,n)
                end do
            end do
     
            !call roverlap(temp_over1, nstat(m), name_mopac(m))
            !call roverlap(temp_over2, nstat(n), name_mopac(n))

            ! Loop over the states
            ii = 1
            do p = 1, m-1 
               ii = ii + nstat(p)
            end do

            do i = 2, nstat(m)+1 
               ii = ii + 1   

               jj = 1
               do q = 1, n-1 
                  jj = jj + nstat(q)
               end do

               do j = 2, nstat(n)+1 
                  jj = jj + 1
           
                  mat_overlap(ii,jj) = temp_over1(i,1) * temp_over2(1,j) * res     
               end do
            end do
            deallocate(temp_over1)
            deallocate(temp_over2)
         end if
      end do
   end do

   ! -> Now, we have to obtain the overlap matrix in adiabatic basis...
   adoverlap = 0._dpr
   if (nc .eq. 0) then
      do i = 1, nstat_tot+1
         adoverlap(i,i) = 1._dpr
      end do
   else
      call adia_overlap(nchrom, eigenvectors, mat_overlap, nstat_tot, adoverlap) 
   end if

   ! -> Writes the overlap matrix in adiabatic basis
   open(newunit=u, file='run_cioverlap.log', status='unknown', form='formatted')
   rewind u
   write(u,*) (nstat_tot+1) 
   do i = 1, nstat_tot+1
      write(u,*) (adoverlap(i,j), j = 1, nstat_tot+1)
   end do
   close(u)

end subroutine calc_ovl

 !
 ! => Read the wave function overlaps from Newton-X outupt:
 ! "run_cioverlap.log".
 !
!subroutine roverlap(overlap, nstat, name_file)
!   integer, parameter :: dpr=kind(1.d0)
!   integer :: i, j, u, nstat
!   character(len=120) :: name_file
!   real(dpr), dimension(:,:), intent(inout) :: overlap

!   open(newunit=u, file=trim(name_file)//"_nx.run_cioverlap.log", &
!        & status='old', form='formatted')
!   rewind u
!   read(u,*)
!   do i = 1, nstat+1
!      read(u,*) (overlap(i,j), j = 1,nstat+1)
!   end do
!   close(u)
!end subroutine roverlap

!
! => This subroutine changes the basis of the overlap matrix
!    DIABATIC TO ADIABATIC
!
subroutine adia_overlap(nchrom, eigenvectors, dioverlap, nstat, adoverlap) 
   integer, parameter :: dpr=kind(1.d0)
   integer :: nchrom, nstat, i, j, u
   real(dpr), dimension(:,:), intent(inout) :: eigenvectors, dioverlap, adoverlap
   real(dpr), allocatable, dimension(:,:) :: a, old_eigenvectors, adj_old_eig

   allocate( old_eigenvectors(nstat+1,nstat+1) )
   old_eigenvectors(:,:) = 0._dpr
   allocate( a(nstat+1,nstat+1) )
   a(:,:) = 0._dpr
   allocate( adj_old_eig(nstat+1,nstat+1) )
   adj_old_eig(:,:) = 0._dpr

   open(newunit=u, file="old_eigenvectors.txt", status='old', & 
        & action='read', form='formatted')
   rewind u
   do i = 1, nstat+1
      read(u,*) (old_eigenvectors(i,j), j = 1,nstat+1)
   end do
   close(u)
   
   adj_old_eig = transpose(old_eigenvectors)

   a = matmul(dioverlap, eigenvectors)
   adoverlap = matmul(adj_old_eig, a)
end subroutine adia_overlap 

end module ovl_mod

