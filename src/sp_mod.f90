module sp_mod

   use en_grad_mod, only: calc_en_grad

   implicit none
   private

   public :: sp_option
   public :: wsp

contains

subroutine sp_option (tresp, numb_atom, isurf, nstat, nat_array, &
                     & name_mopac, out_name, coup_file, nstat_tot, & 
                     & nat_tot, nchrom, dip, gs)

   integer, parameter :: dpr = kind(1.d0)

   integer :: i, j, k 
   integer :: u
   integer :: tresp
   integer :: numb_atom
   integer :: isurf
   integer :: nstat_tot
   integer :: nat_tot
   integer :: nchrom
   integer :: dip
   integer :: gs
   integer :: ind1, ind2
   integer :: ind_tot
   integer :: ibuf

   integer, dimension(:), intent(inout) :: nstat
   integer, dimension(:), intent(inout) :: nat_array

   integer, dimension(:,:), allocatable :: stati_trdip

   character(len=120) :: out_name
   character(len=120) :: coup_file 
   character(len=120) :: cbuf

   character(len=120), dimension(:), intent(inout) :: name_mopac

   real(dpr) :: rbuf

   real(dpr), allocatable, dimension(:) :: vec_en
   real(dpr), allocatable, dimension(:) :: osc_st
   real(dpr), allocatable, dimension(:) :: mat_ene

   real(dpr), allocatable, dimension(:,:) :: ham_tot
   real(dpr), allocatable, dimension(:,:) :: eigenvectors
   real(dpr), allocatable, dimension(:,:) :: grad_total
   real(dpr), allocatable, dimension(:,:) :: trdip_debye
   real(dpr), allocatable, dimension(:,:) :: eci 
   real(dpr), allocatable, dimension(:,:) :: grad_allx, grad_ally, grad_allz
   real(dpr), allocatable, dimension(:,:) :: coordinates

   real(dpr), allocatable, dimension(:,:,:) :: trdip_tot
   real(dpr), allocatable, dimension(:,:,:) :: tr_q

   allocate( vec_en(nstat_tot+1) )
   vec_en(:) = 0._dpr
   allocate( osc_st(nstat_tot) )
   osc_st(:) = 0._dpr
   allocate( mat_ene(nstat_tot) )
   mat_ene(:) = 0._dpr
   allocate( ham_tot(nstat_tot+1, nstat_tot+1) )
   ham_tot(:,:) = 0._dpr
   allocate( eigenvectors(nstat_tot+1, nstat_tot+1) )
   eigenvectors(:,:) = 0._dpr
   allocate( grad_total(numb_atom,3) )
   grad_total(:,:) = 0._dpr
   allocate( trdip_debye(nstat_tot, 3) )
   trdip_debye(:,:) = 0._dpr

   if (gs .gt. 0) then
      isurf = 1
      dip = 0
   end if

   !
   ! => Read the "mopac.exash" and save all the CI energies, gradients, 
   !    transition dipoles and ovl mat
   !
   allocate(eci((nstat_tot+1),nchrom))
   eci(:,:) = 0._dpr
   allocate(grad_allx((nstat_tot*numb_atom),nchrom))
   grad_allx(:,:) = 0._dpr
   allocate(grad_ally((nstat_tot*numb_atom),nchrom))
   grad_ally(:,:) = 0._dpr
   allocate(grad_allz((nstat_tot*numb_atom),nchrom))
   grad_allz(:,:) = 0._dpr
   k = nstat_tot*(nstat_tot+1)/2
   allocate(trdip_tot(k,nchrom,3))
   trdip_tot(:,:,:) = 0._dpr
   allocate(stati_trdip(k,2))
   stati_trdip(:,:) = 0
   allocate(tr_q(nat_tot,nstat_tot,nchrom))
   tr_q(:,:,:) = 0._dpr

   ind1 = 0
   ind2 = 0
   do i = 1, nchrom
      open(newunit=u, file=Trim(Adjustl(name_mopac(i)))//'_nx.exash', &
           &status='old', form='formatted')
      rewind u
      read(u,*) ind1
      do j = 1, ind1
         read(u,*) eci(j,i)
      end do
      read(u,*) ind1, ind2
      ind_tot = ind1*ind2
      do j = 1, ind_tot
         read(u,*) grad_allx(j,i), grad_ally(j,i), grad_allz(j,i)
      end do
      read(u,*) ind1
      do j = 1, ind1
         read(u,*)
      end do
      read(u,*) ind1
      do j = 1, ind1
         read(u,*) stati_trdip(j,1), stati_trdip(j,2), trdip_tot(j,i,1), &
                   & trdip_tot(j,i,2), trdip_tot(j,i,3)
      end do
      read(u,*) ind1, ind2
      do j = 1, ind1
         read(u,*) ibuf, cbuf, (tr_q(j,k,i), k=1,ind2)
      end do
      close(u)
   end do

   !
   ! => Read geometry
   !   
   allocate(coordinates(numb_atom,3))
   coordinates(:,:) = 0._dpr
   open(newunit=u, file="geom", status='old', form='formatted')
   rewind u
   do i = 1, numb_atom
      read(u,*) cbuf, rbuf, (coordinates(i,j), j=1,3), rbuf
   end do
   close(u)


   !
   ! => Call a subroutine which computes the energies, gradients and trans. dip.
   !    moments
   !
   call calc_en_grad(numb_atom, isurf, tresp, nstat, nat_array, name_mopac, &
                     & nstat_tot, nat_tot, nchrom, coup_file, ham_tot, &
                     & vec_en, eigenvectors, grad_total, trdip_debye, osc_st, & 
                     & mat_ene, dip, gs, eci, grad_allx, grad_ally, grad_allz, &
                     & trdip_tot, tr_q, coordinates, stati_trdip)

    ! Write some informations in a easy way to be read from the NX in order 
    ! generate the initial contidions
    open(newunit=u, file='epot_exash', status='unknown', form='formatted')
    rewind u
    do i = 1, nstat_tot+1
        write(u,*) vec_en(i)
    end do 
    close(u)

    open(newunit=u, file='epot_grad', status='unknown', form='formatted')
    rewind u
    do i = 1, numb_atom
        write(u,*) (grad_total(i,j), j = 1, 3)
    end do 
    close(u)

    if (dip .ne. 0) then
       open(newunit=u, file='osc_exash', status='unknown', form='formatted')
       rewind u
       do i = 1, nstat_tot
           write(u,*) osc_st(i)
       end do
       close(u)

       open(newunit=u, file='trdip_exash', status='unknown', form='formatted')
       rewind u
       do i = 1, nstat_tot
           write(u,*) (trdip_debye(i,j), j = 1, 3)
       end do
       close(u)
    end if

    ! Write the output
    call wsp(out_name, tresp, numb_atom, nstat_tot, isurf, ham_tot, mat_ene, &
             & eigenvectors, trdip_debye, osc_st, dip, grad_total, vec_en)
end subroutine sp_option

subroutine wsp(out_name, tresp, numb_atom, nstat_tot, isurf, ham_tot, mat_ene, &
               & eigenvectors, trdip_debye, osc_st, dip, grad_total, vec_en)

   integer, parameter :: dpr = kind(1.d0)

   integer :: i, j 
   integer :: u
   integer :: tresp
   integer :: numb_atom
   integer :: nstat_tot
   integer :: isurf
   integer :: dip

   integer, allocatable, dimension(:) :: indice1, indice2

   character(len=120) :: out_name

   real(dpr), parameter :: eV=27.211386245988

   real(dpr), dimension(:,:), intent(inout) :: ham_tot
   real(dpr), dimension(:,:), intent(inout) :: eigenvectors
   real(dpr), dimension(:,:), intent(inout) :: trdip_debye
   real(dpr), dimension(:,:), intent(inout) :: grad_total

   real(dpr), dimension(:), intent(inout) :: mat_ene
   real(dpr), dimension(:), intent(inout) :: osc_st
   real(dpr), dimension(:), intent(inout) :: vec_en

   open(newunit=u, file=trim(out_name)//'.out', status='unknown', &
        & action='write')
   rewind u
   write(u,*)
   write(u,*) "***************************************************************************"
   write(u,*) "*                       _______  __ ___   _____ __  __                    *"
   write(u,*) "*                      / ____/ |/ //   | / ___// / / /                    *"
   write(u,*) "*                     / __/  |   // /_|| \__ \/ /_/ /                     *" 
   write(u,*) "*                    / /___ /   |/ ___ |___/ / __  /                      *" 
   write(u,*) "*                   /_____//_/|_/_/  |_/____/_/ /_/                       *" 
   write(u,*) "*                                                                         *" 
   write(u,*) "*           EXcitonic Analysis for Surface Hopping dynamics               *" 
   write(u,*) "*                                                                         *" 
   write(u,*) "*                             Eduarda S. Gil                              *" 
   write(u,*) "*                             November 2021                               *" 
   write(u,*) "*                                                                         *" 
   write(u,*) "*                                                                         *" 
   write(u,*) "* => This program calculates:                                             *"
   write(u,*) "*    -> The excitation energies within the exciton model;                 *"
   write(u,*) "*    -> Approximated electronic couplings;                                *"
   write(u,*) "*    -> The gradients for the exciton model;                              *" 
   write(u,*) "*    -> The transition dipole Moments for the exciton Model;              *" 
   write(u,*) "*    -> Diabatic populations                                              *" 
   write(u,*) "*                                                                         *" 
   write(u,*) "***************************************************************************"
   if (tresp .eq. 1) then
      write(u,*)
      write(u,*) "The excitonic coupling between two states on different chromophores will be" 
      write(u,*) "evaluated  using the  transition monopole aproximation (TMA) which is based" 
      write(u,*) "on the transition charges (TrESP)."
   else
      write(u,*)
      write(u,*) "The excitonic coupling between two states on different chromophores will be" 
      write(u,*) "evaluated exactly." 
   end if
   write(u,*)
   write(u,*)

   !
   ! => Print the full Hamiltonian
   !
   write(u,'(a17,i3)')  'Number of states:', (nstat_tot+1)
   write(u,'(a16,i6)')  'Number of atoms:', numb_atom
   write(u,*)
   write(u,*)
   write(u,*) "=> EXCITONIC HAMILTONIAN (a.u) <="
   write(u,*)
   do i = 1, nstat_tot + 1
      write(u,'(*(f20.8))') (ham_tot(i,j), j = 1, nstat_tot + 1)
   end do

   !
   ! => Write the Excitonic Energies 
   !
   write(u,*)
   write(u,*) "=> TOTAL ENERGIES (a.u.) <="
   write(u,*)
   do i = 1, nstat_tot + 1
      write(u,'(2x,f20.9)') vec_en(i) 
   end do
   write(u,*)

   !
   ! => Print eigenvectors
   !
   write(u,*)
   write(u,*) " => EIGENVECTORS OF THE EXCITONIC HAMILTONIAN (a.u.) <= "
   do i = 1, nstat_tot+1
      write(u,'(*(f15.9))') (eigenvectors(i,j), j = 1,nstat_tot+1)
   end do
   write(u,*)

100 format (2x, *(g12.5, 4x))

   !
   ! => Print the gradients
   !
   write(u,*)
   write(u,*) " => GRADIENTS (a.u.) <= "
   do i = 1, numb_atom
      write(u,'(*(f15.9))') (grad_total(i,j), j = 1, 3)
   end do
   write(u,*)

   !
   ! => Print the trdip
   !
   if (dip .gt. 0) then
      allocate( indice1(nstat_tot), indice2(nstat_tot) )
      indice1(:) = 0
      indice2(:) = 0

      do i = 1, nstat_tot
         indice1(i) = i + 1
      end do
      do i = 1, nstat_tot
         indice2(i) = 1
      end do

      write(u,*)
      write(u,*) "                            => Transition dipoles in Debye <="
      write(u,*) "   I  J                                <I| mu |J>"
      do i = 1, nstat_tot
         write(u,'(2x,i3,i3,5x,*(f20.15))') indice2(i), indice1(i), &
                                            & (trdip_debye(i,j), j=1,3)
      end do
      write(u,*) 
      write(u,*) 

      !
      ! => Write the oscillator strenght
      !
      mat_ene = mat_ene * eV
      write(u,*) "  States          Excitation Energy(eV)          Oscillator strength"
      write(u,*)  
      do i = 1, nstat_tot
         write(u,'(2x,i3,i3,9x,f20.15,11x,f20.15)') indice2(i), indice1(i), &
                                                    & mat_ene(i), osc_st(i)
      end do
      write(u,*)
   end if

   write(u,*) '=> EXASH DONE <='

   close(u)

end subroutine wsp

end module sp_mod
