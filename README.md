# EXASH

Exciton Approach for Surface Hopping dynamics.

If you experience problems downloading it, send your request to 
dudasangiogogil@hotmail.com

**How to install EXASH:**

All you need to install EXASH on a Linux system is a Fortran90 
compatible compiler (this release was tested against gfortran 9.4.0). 
The lapack libraries is also required (the versions which come
with your Linux distribution should be sufficient).

In order to obtain the fortran source, type:

> git clone git@gitlab.com:e.sangiogogil/exash_2.0.git

or if you prefear to download the exash.zip, just unzip the distribution:

> unzip exash-main.zip

This should create a new directory called _exash_ that contains 
the directories containing the source files:_scr_ and _utils, and a directory
called _comp_.
Inside the directory _comp_, there is a file called _comp.sh_. 
If you want, you can edit the "INPUT" block at the top of this file. 
Once, you have edited the file _comp.sh_ (or you can use the defaults), 
you just need to run the script _comp.sh_:

> ./comp.sh

At this point, the directory _bin_ should contains all the binary files.

In oder to use the binaries together, set $EXASH environment variable 
to the bin directory of the exash distribution, e.g. if you have 
unpacked exash into your $HOME directory, just set: 

> export EXASH=~/exash/bin (for bourne shell users)

or

> setenv EXASH $HOME/exash/bin (for c-shell type users)


